<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('tags')->insert([
        //     'type' => 'TRANSACTION_STATUS',
        //     'name' => 'Dibatalkan',
        // ]);
        DB::table('tags')->insert([
            'type' => 'TRANSACTION_STATUS',
            'name' => 'Dibayar',
        ]);
        // DB::table('tags')->insert([
        //     'type' => 'TRANSACTION_STATUS',
        //     'name' => 'Tagihan',
        // ]);

        DB::table('tags')->insert([
            'type' => 'ITEM_CATEGORIES',
            'name' => 'Universal',
        ]);
        DB::table('tags')->insert([
            'type' => 'ITEM_CATEGORIES',
            'name' => 'Spoiler',
        ]);
        DB::table('tags')->insert([
            'type' => 'ITEM_CATEGORIES',
            'name' => 'Oli',
        ]);

        DB::table('tags')->insert([
            'type' => 'SERVICE_CATEGORIES',
            'name' => 'Cuci Steam',
        ]);

        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Universal',
        ]);
        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Mitsubishi',
        ]);
        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Suzuki',
        ]);
        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Nissan',
        ]);
        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Daihatsu',
        ]);
        DB::table('tags')->insert([
            'type' => 'CAR_VENDOR',
            'name' => 'Toyota',
        ]);

    }
}
