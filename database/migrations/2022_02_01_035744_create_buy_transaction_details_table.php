<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_transaction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('buy_transaction_id');
            $table->bigInteger('item_id')->nullable();
            $table->bigInteger('services_id')->nullable();
            $table->bigInteger('qty')->nullable();
            $table->bigInteger('price')->nullable();
            $table->boolean('is_deleted')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_transaction_details');
    }
}
