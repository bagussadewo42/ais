<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBuyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('buy_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('invoice_no',15)->unique();
            $table->string('supplier_name')->nullable();
            $table->string('supplier_phone')->nullable();
            $table->dateTime('date');
            $table->bigInteger('type');
            $table->bigInteger('status');
            $table->bigInteger('total_price');
            $table->bigInteger('discount_price')->nullable();
            $table->boolean('is_canceled')->nullable();
            $table->boolean('is_deleted')->nullable();
            $table->string('notes')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('buy_transactions');
    }
}
