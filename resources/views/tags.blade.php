@extends('layouts.master')
@section('title', 'Tags')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .lebar {
            width: 600px !important;
        }
    </style>
@endsection
@section('content')
<div id="success-alert" class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <strong>Success !</strong> Data has been saved
</div>
<div id="error-alert" class="alert alert-error" role="alert" style="display: none;">
  </button>
  <strong>Error !</strong> Something Happened
</div>

<div class="row">
    <table id="tags" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Type</th>
                <th>Subtype</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<div class="row">
    <button type="button" id="btnAdd" class="btn btn-primary" data-toggle="modal" data-target="#addItem">Add New Tags</button>
</div>

<!-- MODAL ADD -->
<div id="addItem" class="modal fade bs-example-modal-lg" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Add New Tags</h4>
      </div>
      <div class="modal-body">
          <form id="add-item" class="form-horizontal form-label-left" data-toggle="validator">
            <input type="hidden" name="action" id="action" value="">
            <input type="hidden" name="tags-id" value="" id="tags-id">
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tags-name">Tags Name <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="tags-name" name="tags-name" required="required" class="form-control col-md-7 col-xs-12" autofocus>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tags-type">Tags Type <span class="required">*</span>
              </label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <!-- <input type="text" id="tags-type" name="tags-type" required="required" class="form-control col-md-7 col-xs-12"> -->
                <select name="tags-type", id="tags-type" class="form-control" required="">
                  <option value=""></option>
                  <option value="CAR_VENDOR">Mobil</option>
                  <option value="ITEM_CATEGORIES">Kategori Produk</option>
                  <option value="SERVICE_CATEGORIES">Kategori Jasa</option>
                  <!-- <option value="TRANSACTION_STATUS">Status</option> -->
                </select>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-3 col-sm-3 col-xs-12" for="tags-subtype">Tags Subtype</label>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="text" id="tags-subtype" name="tags-subtype" class="form-control col-md-7 col-xs-12" readonly="">
              </div>
            </div>
            <div class="form-group">
              <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                <input type="submit" name="submit" id="add-submit" class="btn btn-primary" value="Submit">
                 <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
                <button class="btn btn-info"  data-dismiss="modal" type="button">Cancel</button>
                <button class="btn btn-default" type="reset">Reset</button>
                <button id="btnDel" class="btn btn-danger" type="button" style="display: none;">Delete</button>
              </div>
            </div>
          </form>
      </div>
      <div class="modal-footer">
        <span class="required">*</span><i>Subtype are filled when type has same parent</i>
      </div>
    </div>
  </div>
</div>

@endsection
@section('scripts')
<!-- Datatables -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
        $("#btnAdd").click(function(){
          $('#tags-id').val('');
          $('#tags-name').val('');
          $('#tags-type').val('');
          $('#tags-subtype').val('');
        });
        var table = $('#tags').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: 'tags/anytag',
            columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "width": "1%", 
                    orderable: false, 
                    searchable:false,
                    "className": "text-center"
                },
                {data: 'name', name: 'name'},
                {data: 'type', name: 'type'},
                {data: 'subtype', name: 'subtype'},
                {data: 'action', orderable: false, searchable:false, name:'id' }
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all"
            }],
            "order": [[ 2, 'asc' ], [ 1, 'asc' ]] 
        });
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $('modal').on('shown.bs.modal', function(){
          $(this).find('[autofocus]').focus();
        });

        $('#addItem').submit(function (e) {
            var url = 'add';
            var action = $("#action").val();
            if (action == 'edit' ) url = 'edit'
            var data = {
              id: $("#tags-id").val(),
              name: $("#tags-name").val(),
              type: $("#tags-type").val(),
              subtype: $("#tags-subtype").val(),
            };
            $.post('tags/' + url, data, function(data,status) {
                table.ajax.reload();
                $(".bs-example-modal-lg").modal('hide');
                $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
                $("#action").val('');
            })
              .fail(function(){
                $(".bs-example-modal-lg").modal('hide');
                $("#error-alert").fadeTo(3000, 500);
              });
            $(this).find("input[type=text], textarea").val(""); // Clear  
            e.preventDefault();
        });

        $('#addItem').on('hidden.bs.modal', function () {
            $("#btnDel").hide();
        });

        $("#btnDel").click(function() {
            var id = $("#tags-id").val();
            var data = {
              id:id
            }
            $.post('tags/delete', data, function(data, status) {
                table.ajax.reload();
                $(".bs-example-modal-lg").modal('hide');
                $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
                $("#action").val('');
            })
              . fail(function() {
                $(".bs-example-modal-lg").modal('hide');
                $("#error-alert").fadeTo(3000, 500);
              });
            alert('id is : ' + id);
            e.preventDefault();
        });
    });
      
   // Edit row
  function editRow(id) {
    if ( 'undefined' != typeof id ) {
      $.getJSON('tags/edit/' + id, function(obj) {
        $('#tags-id').val(id);
        $('#tags-name').val(obj.name);
        $('#tags-type').val(obj.type);
        $('#tags-subtype').val(obj.subtype);
        $('#action').val('edit');
        $("#btnDel").show();
        $('#addItem').modal('show');
      }).fail(function() { alert('Unable to fetch data, please try again later.') });
    } else alert('Unknown row id.');
  }
</script>

@endsection