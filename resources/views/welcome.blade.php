@extends('layouts.master')
@section('title', 'Dashboard')
@section('styles')
  <style>
    #revenue {
      font-size: 20px!important;
      line-height: 3.1;
    }
  </style>
@endsection
@section('content')
<div class="row">
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-cubes"></i></div>
      <div class="count">{{ $items }}</div>
      <h3>Total Produk</h3>
      <p>Semua produk yang tersimpan.</p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-gear"></i></div>
      <div class="count">{{ $services }}</div>
      <h3>Total Jasa</h3>
      <p>Semua jasa yang tersimpan.</p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-exchange"></i></div>
      <div class="count">{{ $transactions }}</div>
      <h3>Total Transaksi</h3>
      <p>Semua transaksi yang terbuat.</p>
    </div>
  </div>
  <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-dollar"></i></div>
      <div class="count" id="revenue">Rp. </div>
      <h3>Pendapatan / Minggu</h3>
      <p>Total pendapatan per minggu dalam Rp.</p>
    </div>
  </div>
</div>
<div class="row" style="padding-top: 5%;">
  <div class="col-md-6">
    <canvas id="sales" width="800" height="450"></canvas>
  </div>
  <div class="col-md-4">
    <!-- <div style="height: 50px;"></div> -->
    <canvas id="doughnut-chart" width="800" height="450"></canvas>
  </div>
  <div class="col-md-2">
    <h4>Produk yang hampir habis</h4>
    <div class="ln_solid"></div>
    <ol>
      @forelse ($out_of_stock as $item)
          <li>{{ $item->name }}</li>
      @empty
          
      @endforelse
    </ol>
  </div>
</div>
@endsection
@section('scripts')
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script> -->
<script src="{{ asset('js/Chart.bundle.js') }}"></script>
<script src="{{ asset('js/numeral/numeral.min.js') }}"></script>
<script type="text/javascript">
  $(document).ready(function() {
    var rev = {!! json_encode($revenue) !!};
    var revenue = numeral(rev[0].total).format(0,0);
    $("#revenue").append(revenue);
    var sales = {!! json_encode($sales) !!};
    var ratios = {!! json_encode($ratio) !!};
    var dates = [];
    var total = [];
    var ratio_label = [];
    var ratio = [];
    for (var i = 0; i < sales.length; i++) {
      dates.push(sales[i].dates);
      total.push(sales[i].total);
    }
    for (var i = 0; i < ratios.length; i++) {
      ratio_label.push(ratios[i].type);
      ratio.push(ratios[i].total);
    }
    var c = new Chart(document.getElementById("sales"), {
    type: 'line',
    data: {
      // labels: [1500,1600,1700,1750,1800,1850,1900,1950,1999,2050],
      labels: dates,
      datasets: [{ 
          // data: [86,114,106,106,107,111,133,221,783,2478],
          data: total,
          label: "Total",
          borderColor: "#3e95cd",
          fill: false
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Total penjualan per minggu (dalam Rp.)'
      }
    }
  });

  var d = new Chart(document.getElementById("doughnut-chart"), {
    type: 'doughnut',
    data: {
      labels: ratio_label,
      datasets: [
        {
          label: "Products Ratio",
          backgroundColor: ["#3e95cd", "#e8c3b9"],
          data: ratio
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Penjualan rasio produk dengan jasa'
      }
    }
  });
  });
</script>
@endsection