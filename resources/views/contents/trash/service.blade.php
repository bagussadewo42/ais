@extends('layouts.master')
@section('title', 'Items')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@include('shared.message')
<div id="success-alert" class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <strong>Success !</strong> Data has been restore
</div>
<div id="error-alert" class="alert alert-error" role="alert" style="display: none;">
  </button>
  <strong>Error !</strong> Something Happened
</div>
<div class="row">
    <table id="services_grid" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>Category</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
    </table>
</div>
<!-- MODAL RESTORE -->
<div id="restoreItem" class="modal fade bs-example-modal-sm" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Restore Item</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" name="item_id" id="item_id" value="">
          <h4>Are you sure want restore this item ?</h4>
          <div class="text-center">
              <button class="btn btn-success" id="btnRestore">Restore</button>
              <button class="btn btn-info"  data-dismiss="modal" type="button">Cancel</button>
          </div>
      </div>
      <div class="modal-footer">
        
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<!-- Datatables -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    $(document).ready(function() {
         var table = $('#services_grid').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: '{{ url('trash/anyservicetrash') }}',
            columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "width": "1%", 
                    orderable: false, 
                    searchable:false,
                    "className": "text-center"
                },
                {data: 'name', name: 'name'},
                {data: 'category', name: 'category'},
                {data: 'price', name: 'price', render: $.fn.dataTable.render.number('.','.',0,'Rp. ')},
                {data: 'action', orderable: false, searchable:false, name:'id' }
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all",
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "order": [[ 1, 'asc' ]]
        });
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        $("#btnRestore").click(function(e) {
            var id = $("#item_id").val();
            var data = {
              id:id,
              _token : "{{ csrf_token() }}"
            }
            $.post('services/restore', data, function(data, status) {
                table.ajax.reload();
                $(".bs-example-modal-lg").modal('hide');
                $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
                    $("#success-alert").slideUp(500);
                });
                $("#action").val('');
            })
              . fail(function() {
                $(".bs-example-modal-lg").modal('hide');
                $("#error-alert").fadeTo(3000, 500);
              });
            console.log(id);
            $("#restoreItem").modal('hide');
            e.preventDefault();
        });
    });

    function restore(id) {
        if ( 'undefined' != typeof id ) {
            $('#item_id').val(id);
            $('#restoreItem').modal('show');
        } else alert('Unknown row id.');
    }
</script>
@endsection