@extends('layouts.master')
@section('title', 'Produk')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <style>
        .deleted-record {
            color:red;
            font-style: italic;
            text-decoration: line-through;
        }
    </style>
@endsection
@section('content')
@include('shared.message')
<div class="row">
        <table id="examples" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Nama Produk</th>
                    <th>Kategori</th>
                    <th>Mobil</th>
                    <th>Harga Beli terakhir</th>
                    <th>Harga Jual</th>
                    <th>Stok</th>
                    <th>Aksi</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="row">
        <a href="{{ url('items/create') }}" class="btn btn-default">Tambah Produk</a>
    </div>
@endsection
@section('scripts')
<!-- Datatables -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script>
  var table = $('#examples').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: '{{ url('items/anyitem') }}',
            columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    },
                    "width": "1%", 
                    orderable: false, 
                    searchable:false,
                    "className": "text-center"
                },
                {data: 'name', name: 'name'},
                {data: 'category', name: 'category'},
                {data: 'applied_car', name: 'applied_car'},
                {data: 'last_buy_price', name: 'last_buy_price', render: $.fn.dataTable.render.number('.','.',0,'Rp. ')},
                {data: 'price', name: 'price', render: $.fn.dataTable.render.number('.','.',0,'Rp. ')},
                {data: 'stock', name: 'stock'},
                {data: 'action', orderable: false, searchable:false, name:'id' }
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all",
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "order": [[ 1, 'asc' ]],
            "createdRow": function( row, data, dataIndex ) {
                if ( data.is_deleted ) {        
                    $(row).addClass('deleted-record');
                }
            }
        });
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();
        $(".alert").fadeTo(3000, 500).slideUp(500, function(){
            $(".alert").slideUp(500);
        });
</script>
@endsection