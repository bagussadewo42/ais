@extends('layouts.master')
@section('title', 'Edit Item')
@section('styles')
    <link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
@php
	$ok = false;
@endphp
	<div class="row">
		<form id="add-item" class="form-horizontal form-label-left" data-toggle="validator" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Produk <span class="required">*</span>
	          </label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="text" id="name" name="name" class="form-control col-md-7 col-xs-12" value="{{ $item->name }}" autofocus required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <select class="form-control" id="category" name="category" required>
	              <option value="" >Pilih Kategori</option>  
	                  @foreach($categories as $category)
	                      <option value="{{ $category->id }}" {{ $category->id == $item->category ? 'selected="selected"' : '' }}>{{ $category->name }}</option>    
	                  @endforeach
	            </select>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobil <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <select class="form-control" id="car" name="car[]" multiple="multiple" required style="width: 100%">
	                  @foreach($cars as $car)
	                  	@foreach($cars_selected as $selected)
							@if($car->name == $selected)
								@php
									$ok = true;
									break;
								@endphp
							@endif
	                  	@endforeach
	                      <option value="{{ $car->name }}" {{ $ok == true ? 'selected' : '' }}>{{ $car->name }}</option>
							@php
								$ok = false;
							@endphp
	                  @endforeach
	            </select>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="item-last_buy_price" class="control-label col-md-3 col-sm-3 col-xs-12">Harga Beli terakhir <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="last_buy_price" class="form-control col-md-7 col-xs-12" name="last_buy_price" value="{{ $item->last_buy_price }}" data-a-sign="Rp. " required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="item-price" class="control-label col-md-3 col-sm-3 col-xs-12">Harga Jual <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="price" class="form-control col-md-7 col-xs-12" name="price" value="{{ $item->price }}" data-a-sign="Rp. " required>
	          </div>
	        </div>
	        <!-- <div class="form-group">
	          <label for="item-stock" class="control-label col-md-3 col-sm-3 col-xs-12">Stock <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="stock" class="form-control col-md-7 col-xs-12" type="number" value="{{ $item->stock }}" name="stock" required>
	          </div>
	        </div> -->
	        <div class="form-group">
	          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	            <input type="submit" name="submit" id="add-submit" class="btn btn-primary" value="Simpan">
	             <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
	            <a class="btn btn-info" href="{{ url('items') }}">Batal</a>
      			</form>
	            <!-- <button class="btn btn-danger" type="button">Delete</button> -->
				@if($item->is_deleted == TRUE)
					<form method="post" action="{{ action('ItemController@restore', $item->id) }}" class="pull-left">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="#" id="btnRes" class="btn btn-embossed btn-success btn-del-res">Kembalikan</a>&nbsp;
                    </form>
				@else
                    <form method="post" action="{{ action('ItemController@destroy', $item->id) }}" class="pull-left">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <a href="#" id="btnDel" class="btn btn-embossed btn-danger btn-del-res">Hapus</a>&nbsp;
                    </form>
				@endif
	          </div>
	        </div>
	</div>
@endsection
@section('scripts')
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script src="{{ asset('js/validator.js') }}"></script>
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $("#price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        $("#last_buy_price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        $('#car').select2();
        // $('#car').val('Universal').trigger('change');
        $(document).on("click", ".btn-del-res", function(e) {
	        var $form = $(this).closest('form');
			var id = $(this).attr('id');
			var state = id =='btnRes'? 'mengembalikan' : 'menghapus';
	        bootbox.confirm({
	            message: 'Kamu yakin mau '+state+' ini ?',
	            size: 'small',
	            buttons: {
	                confirm: {
	                    label: 'Ya',
	                    className: 'btn btn-embossed btn-primary'
	                },
	                cancel: {
	                    label: 'Tidak',
	                    className: 'btn btn-embossed btn-danger'
	                }
	            },
	            callback: function(result) {
	                if(result) {
	                    $form.trigger('submit');
	                }
	            }
	        });
	    });
	});
</script>
@endsection