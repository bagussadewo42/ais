@extends('layouts.master')
@section('title', 'Tambah Produk')
@section('styles')
    <link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
@endsection
@section('content')
	<div class="row">
		<form id="add-item" class="form-horizontal form-label-left" data-toggle="validator" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Nama Produk <span class="required">*</span>
	          </label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="text" id="name" name="name" class="form-control col-md-7 col-xs-12" autofocus required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <select class="form-control" id="category" name="category" required>
	              <option value="" >Pilih Kategori</option>  
	                  @foreach($categories as $category)
	                      <option value="{{ $category->id }}" >{{ $category->name }}</option>    
	                  @endforeach
	            </select>
	          </div>
	        </div>
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12">Mobil <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <select class="form-control" id="car" name="car[]" multiple="multiple" required style="width: 100%">
	              @foreach($cars as $car)
                      <option value="{{ $car->name }}" >{{ $car->name }}</option>    
                  @endforeach
	            </select>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="item-last_buy_price" class="control-label col-md-3 col-sm-3 col-xs-12">Harga Beli terakhir <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="last_buy_price" class="form-control col-md-7 col-xs-12" name="last_buy_price" data-a-sign="Rp. " required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="item-price" class="control-label col-md-3 col-sm-3 col-xs-12">Harga Jual <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="price" class="form-control col-md-7 col-xs-12" name="price" data-a-sign="Rp. " required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="item-stock" class="control-label col-md-3 col-sm-3 col-xs-12">Stok <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input id="stock" class="form-control col-md-7 col-xs-12" type="number" name="stock" required>
	          </div>
	        </div>
	        <div class="form-group">
	          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	            <input type="submit" name="submit" id="add-submit" class="btn btn-primary" value="Simpan">
	             <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
	            <a class="btn btn-info" href="{{ url('items') }}">Batal</a>
	            <button class="btn btn-default" type="reset">Reset</button>
	          </div>
	        </div>
      </form>
	</div>
@endsection
@section('scripts')
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script src="{{ asset('js/validator.js') }}"></script>
<script type="text/javascript">
	$(document).ready(function() {
        $("#price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        $("#last_buy_price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        $('#car').select2();
        // $('#car').val('Universal').trigger('change');
	});
</script>
@endsection