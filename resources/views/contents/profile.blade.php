@extends('layouts.master')
@section('title', 'Profile')
@section('content')
<div class="row">
  <div class="col-md-3"></div>
  <div class="col-md-6">
	<div class="row">
		@if ($message = Session::get('success'))
		<div class="alert alert-success">
		  <button type="button" class="close" data-dismiss="alert">×</button> 
		        <strong>{{ $message }}</strong>
		</div>
		@endif

		@if ($message = Session::get('error'))
		<div class="alert alert-danger alert-block">
		  <button type="button" class="close" data-dismiss="alert">×</button> 
		        <strong>{{ $message }}</strong>
		</div>
		@endif
		<form id="add-item" class="form-horizontal form-label-left" data-toggle="validator" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
	        <div class="form-group">
	          <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Item Name <span class="required">*</span>
	          </label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="text" id="name" name="name" class="form-control col-md-7 col-xs-12" value="{{ Auth::user()->name }}" autofocus required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="email" class="control-label col-md-3 col-sm-3 col-xs-12">Email <span class="required">*</span></label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="email" id="email" class="form-control col-md-7 col-xs-12" name="email" value="{{ Auth::user()->email }}" readonly required>
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="old_password" class="control-label col-md-3 col-sm-3 col-xs-12">Old Password </label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="password" id="old_password" class="form-control col-md-7 col-xs-12" name="old_password">
	          </div>
	        </div>
	        <div class="form-group">
	          <label for="new_password" class="control-label col-md-3 col-sm-3 col-xs-12">New Password </label>
	          <div class="col-md-6 col-sm-6 col-xs-12">
	            <input type="password" id="new_password" class="form-control col-md-7 col-xs-12" name="new_password">
	          </div>
	        </div>
	        <div class="form-group">
	          <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
	            <input type="submit" name="submit" id="add-submit" class="btn btn-primary" value="Submit">
	             <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
	            <a class="btn btn-info" href="{{ url('items') }}">Cancel</a>
	            <button class="btn btn-default" type="reset">Reset</button>
	          </div>
	        </div>
      </form>
	</div>
  </div>
  <div class="col-md-5"></div>
</div>
@endsection