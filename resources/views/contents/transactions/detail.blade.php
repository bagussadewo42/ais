@extends('layouts.master')
@section('title', 'Detail Transaksi')
@section('styles')
    <link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" /> -->
    <link href="{{ asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}} " rel="stylesheet">
    <style type="text/css">
    	th {
    		text-align: center;
    	}
    	.select2 {
    		width: 410px!important;
    	}
    	#btnCancel {
			cursor: pointer;
			display: inline-block;
			position: relative;
			transition: 0.5s;
    	}
    	#btnCancel:hover {
    		padding-right: 25px;
    	}
    	#btnCancel .ctext {
    		display: none;
    	}
    	#btnCancel:hover .ctext {
    		display: inline;
    		font-style: bold;
    	}
    </style>
@endsection
@section('content')
<!-- {{ $transaction }}
{{ $transaction_details }} -->

	<div class="row">
		<div class="col-md-10">
			<h2>
				<a id="btnCancel" href="{{ url('transactions') }}" class="btn btn-info">
					<i class="fa fa-arrow-circle-left"></i>
					<span class="ctext"> Kembali</span>
				</a>
				{{ $transaction->invoice_no }}
			</h2>
		</div>
		<div class="col-md-2">
			<div class="form-group">
              <label>Tanggal Transaksi</label>
              <input id="transDate" name="transDate" type="text" class="form-control" value="{{ $transaction->date }}" readonly>
            </div>
		</div>
	</div>
	<div class="row" style="padding-top: 110px;">
		<table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10%;"></th>
              <th style="width: 25%;">Nama Produk</th>
              <th style="width: 10%;">Qty</th>
              <th style="width: 15%;">Stok</th>
              <th style="width: 15%;">Harga</th>
              <th style="width: 15%;">Total</th>
              <th style="width: 5%;"></th>
            </tr>
          </thead>
          <tbody id="itemsContainer">
          	@foreach($transaction_details as $item)
          		@if($item->item_id == 0)
					@continue
          		@endif
				<tr class="itemBox{{ $item->id }}" item-id="{{ $item->id }}">
					<td class="itemImage text-center"><span class="fa fa-shopping-cart"></span></td>
					<td class="itemName">
						<select class="form-control itemSearch" data-id-item="{{ $item->id }}" name="itemId[]" disabled="">
							@foreach($items as $it)
		                      <option value="{{ $it->id }}" {{ $it->id == $item->item_id ? 'selected="selected"' : '' }}>{{ $it->name }}</option>    
		                  	@endforeach
						</select>
					</td>
					<td class="itemQty">
						@foreach($items as $it)
							@if($it->id == $item->item_id)
								<input type="number" class="form-control itemQtyInput" name="itemQty[]" max="{{ $it->stock+$item->qty }}" 
								min="1" value="{{ $item->qty }}" readonly>
								@break
							@endif
						@endforeach
					</td>
					<td class="itemAvailable text-center">
						@foreach($items as $it)
							@if($it->id == $item->item_id)
								<span>{{ $it->stock }}</span>
								@break
							@endif
						@endforeach
					</td>
					<td class="itemPrice text-center"><span>{{ $item->price }}</span><input type="hidden" name="itemPrice[]" value=""></td>
					<td class="itemTotal text-center"><span>{{ $item->price*$item->qty }}</span></td>
					<!-- <td class="text-center"><a href="#" class="itemDrop btn btn-rounded btn-danger btn-xs"><i class="fa fa-close"><i></a></td> -->
				</tr>
          	@endforeach
          </tbody>
          <!-- <tbody>
            <tr>
            	<td colspan="7" class="text-center"><button id="addItem" class="btn btn-default"> + Add Item</button></td>
            </tr>
          </tbody> -->
        </table>
	</div>
	<div class="ln_solid"></div>
	<div class="row" style="padding-top: 0px;">
		<table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10%;"></th>
              <th style="width: 25%;">Nama Jasa</th>
              <th style="width: 10%;"></th>
              <th style="width: 15%;"></th>
              <th style="width: 15%;">Harga</th>
              <th style="width: 15%;"></th>
              <th style="width: 5%;"></th>
            </tr>
          </thead>
          <tbody id="servicesContainer">
          	@foreach($transaction_details as $service)
				@if($service->services_id == 0)
					@continue
				@endif
				<tr class="serviceBox{{ $service->id }}" service-id="{{ $item->id }}">
					<td class="serviceImage text-center"><span class="fa fa-gears"></span></td>
					<td class="serviceName">
						<select class="form-control serviceSearch" data-id-item="{{ $service->id }}" name="serviceId[]" disabled="">
							@foreach($services as $se)
								<option value="{{ $se->id }}" {{ $se->id == $service->services_id ? 'selected="selected"' : '' }}>{{ $se->name }}</option>
							@endforeach
						</select>
					</td>
					<td class="serviceQty"></td>
					<td class="serviceAvailable text-center"><span></span></td>
					<td class="servicePrice text-center"><span>{{ $service->price }}</span><input type="hidden" name="servicePrice[]" value=""></td>
					<td class="serviceTotal text-center"><span></span></td>
					<!-- <td class="text-center"><a href="#" class="itemDrop btn btn-rounded btn-danger btn-xs"><i class="fa fa-close"><i></a></td> -->
				</tr>
          	@endforeach
          </tbody>
          <!-- <tbody>
            <tr>
            	<td colspan="7" class="text-center"><button id="addService" class="btn btn-default"> + Add Service</button></td>
            </tr>
          </tbody> -->
        </table>
	</div>
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4"></div>
		<div class="col-md-4">
		<div class="form-group">
			<label for="item-discount_price" class="control-label col-md-5 col-sm-5 col-xs-12">Potongan harga</label>
			<div class="col-md-6 col-sm-6 col-xs-12">
			<input id="discount_price" class="form-control col-md-7 col-xs-12" name="discount_price" data-a-sign="Rp. " value="{{ $transaction->discount_price }}" readonly>
			</div>
		</div>
		</div>
	</div>
	<div class="row">
		<div class="ln_solid"></div>
		<div class="col-md-4">
			<textarea name="note" id="note" class="form-control" placeholder="Note Here" rows="5" readonly>{{ $transaction->notes }}</textarea>
		</div>
		<div class="col-md-4">
			<p>
				<b>Catatan : </b>jika produk atau jasa belum ada, tambahkan <a href="{{ url('items') }}"><i>Produk</i></a> atau 
				<a href="{{ url('services') }}"> <i>Jasa</i></a> terlebih dahulu.
			</p>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-7">
					<div class="text-right">
						<p>
							<b>Sub Total</b>
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<p id="subTotal">
						<b>Rp. 0</b>
					</p>
					<div class="ln_solid"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
		          <select name="status" id="status" class="form-control" required="" disabled>
		            <option value="">Choose Status</option>  
	                  @foreach($status as $stat)
	                      <option value="{{ $stat->id }}" {{ $stat->id == $transaction->status ? 'selected="selected"' : '' }}>{{ $stat->name }}</option>    
	                  @endforeach
		          </select>   
				</div>
					@if($transaction->is_deleted == false)
					<div class="col-md-3">
						<form method="POST" action="{{ action('TransactionsController@destroy', $transaction->id) }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<a href="#" id="btnDel" class="btn btn-embossed btn-danger">Hapus</a>&nbsp;
						</form>
					</div>
					<div class="col-md-3">
						<a target="_blank" class="btn btn-embossed btn-info" href="{{ url('transactions/detail/print', [$transaction->id]) }}">
							<span class="glyphicon glyphicon-print"></span> Cetak 
						</a>
					</div>
					@endif
			</div>
		</div>
	</div>
<!-- MODAL INFO -->
<div id="info" class="modal fade bs-example-modal-sm" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
          <div id="minfo" class="row">
            <div class="text-center">
              <p>Transaksi Berhasil dihapus</p>
              <p><a href="{{ url('transactions') }}" class="btn btn-success">Konfirmasi</a></p>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('vendors/select2/dist/js/select2.full.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script> -->
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script src="{{ asset('js/validator.js') }}"></script>
<script src="{{ asset('vendors/moment/moment.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/numeral/numeral.min.js') }}"></script>
<script src="{{ asset('js/bootbox.min.js') }}"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script> -->
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script type="text/javascript">
	var data = {!! json_encode($transaction_details->toArray()) !!};	
  	var id = data[data.length-1]['id']+1;
	$(document).ready(function() {
		var tdate = moment("{{ $transaction->date }}");
		$("#transDate").datetimepicker({
			date: tdate
		});
    	$("#discount_price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
		calSummary();
		var data = {!! json_encode($transaction_details->toArray()) !!};
	});

	
	$(document).on("click", "#btnDel", function(e) {
	        var $form = $(this).closest('form');
	        bootbox.confirm({
	            message: 'Kamu yakin mau menghapus ini ?',
	            size: 'small',
	            buttons: {
	                confirm: {
	                    label: 'Ya',
	                    className: 'btn btn-embossed btn-primary'
	                },
	                cancel: {
	                    label: 'Tidak',
	                    className: 'btn btn-embossed btn-danger'
	                }
	            },
	            callback: function(result) {
	                if(result) {
	                    $form.trigger('submit');
	                }
	            }
	        });
	    });

	function calSummary() {
        var subTotal = 0;
        var val = 0;
        var vals = 0;
        $(".itemTotal span").each(function (e) {
            val = $(this).text();
            if(val == '-') val = '0';
			val = val.replace(/,/g,'');
            subTotal = subTotal + parseInt(val);
        });
        $(".servicePrice span").each(function (e) {
            vals = $(this).text();
            if(vals == '-') vals = '0';
			vals = vals.replace(/,/g,'');
            subTotal = subTotal + parseInt(vals);
        });
        subTotal -= get_discount_price();
        $("#subTotal b").text("Rp. " + numeral(subTotal).format(0,0));              
	}

	function get_discount_price() {
		var discount_price = $("#discount_price").val() || '-';
		if(discount_price == '-') discount_price = '0';
		return parseInt(discount_price.replace(/[^0-9]/g, ''));
	}
</script>
@endsection