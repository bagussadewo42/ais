@extends('layouts.master')
@section('title', 'Tambah Transaksi Pembelian')
@section('styles')
    <link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" /> -->
    <link href="{{ asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css')}} " rel="stylesheet">
    <style type="text/css">
    	th {
    		text-align: center;
    	}
    	.select2 {
    		width: 410px!important;
    	}
      #btnCancel {
        cursor: pointer;
        display: inline-block;
        position: relative;
        transition: 0.5s;
      }
      #btnCancel:hover {
        padding-right: 25px;
      }
      #btnCancel .ctext {
        display: none;
      }
      #btnCancel:hover .ctext {
        display: inline;
        font-style: bold;
      }
    </style>
@endsection
@section('content')
<form method="POST" class="form-horizontal form-label-left" action="{{ url('buy_transactions/save') }}">
	<div class="row">
		<div class="col-md-10">
			<h2>
        <a id="btnCancel" href="{{ url('buy_transactions') }}" class="btn btn-info">
          <i class="fa fa-arrow-circle-left"></i>
          <span class="ctext"> Kembali</span>
        </a>
        {{ $invoice }}
      </h2>
		</div>
		<div class="col-md-2">
			<div class="form-group">
              <label>Tanggal Transaksi</label>
              <input id="transDate" name="transDate" type="text" class="form-control">
            </div>
		</div>
	</div>
  <div class="row">
    <div class="form-group">
      <label for="supplier_name" class="control-label col-md-2 col-sm-2 col-xs-4">Nama Supplier <span class="required">*</span></label>
      <div class="col-md-4 col-sm-4 col-xs-6">
        <input id="supplier_name" class="form-control col-md-4 col-xs-6" type="text" name="supplier_name" required>
      </div>
    </div>
    
    <div class="form-group">
      <label for="supplier_phone" class="control-label col-md-2 col-sm-2 col-xs-4">No. Hp Supplier</label>
      <div class="col-md-4 col-sm-4 col-xs-6">
        <input id="supplier_phone" class="form-control col-md-4 col-xs-6" type="text" name="supplier_phone">
      </div>
    </div>
  </div>
	<div class="row" style="padding-top: 50px;">
		<table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10%;"></th>
              <th style="width: 25%;">Nama Produk</th>
              <th style="width: 10%;">Qty</th>
              <th style="width: 15%;">Stok</th>
              <th style="width: 15%;">Harga</th>
              <th style="width: 15%;">Total Harga</th>
              <th style="width: 5%;"></th>
            </tr>
          </thead>
          <tbody id="itemsContainer">
          </tbody>
          <tbody>
            <tr>
            	<td colspan="7" class="text-center"><button id="addItem" class="btn btn-default"> + Tambah Produk</button></td>
            </tr>
          </tbody>
        </table>
	</div>
	<div class="ln_solid"></div>
	<div class="row" style="padding-top: 0px;">
		<table class="table table-sm">
          <thead>
            <tr>
              <th style="width: 10%;"></th>
              <th style="width: 25%;">Jasa</th>
              <th style="width: 10%;"></th>
              <th style="width: 15%;"></th>
              <th style="width: 15%;">Harga</th>
              <th style="width: 15%;"></th>
              <th style="width: 5%;"></th>
            </tr>
          </thead>
          <tbody id="servicesContainer">
          </tbody>
          <tbody>
            <tr>
            	<td colspan="7" class="text-center"><button id="addService" class="btn btn-default"> + Tambah Jasa</button></td>
            </tr>
          </tbody>
        </table>
	</div>
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4"></div>
    <div class="col-md-4">
      <div class="form-group">
        <label for="item-discount_price" class="control-label col-md-5 col-sm-5 col-xs-12">Potongan harga</label>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <input id="discount_price" class="form-control col-md-7 col-xs-12" name="discount_price" data-a-sign="Rp. ">
        </div>
      </div>
    </div>
  </div>
	<div class="row">
		<div class="ln_solid"></div>
		<div class="col-md-4">
			<textarea name="note" id="note" class="form-control" placeholder="Catatan transaksi disini ..." rows="5"></textarea>
		</div>
		<div class="col-md-4">
			<p>
				<b>Catatan : </b>jika produk atau jasa belum ada, tambahkan <a href="{{ url('items') }}"><i>Produk</i></a> atau 
				<a href="{{ url('services') }}"> <i>Jasa</i></a> terlebih dahulu.
			</p>
		</div>
		<div class="col-md-4">
			<div class="row">
				<div class="col-md-7">
					<div class="text-right">
						<p>
							<b>Sub Total</b>
						</p>
					</div>
				</div>
				<div class="col-md-4">
					<p id="subTotal">
						<b>Rp. 0</b>
					</p>
          <div class="ln_solid"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-5">
          <select name="status" id="status" class="form-control" required="">
            <option value="">Pilih Status</option>  
                @foreach($status as $stat)
                    <option value="{{ $stat->id }}" >{{ $stat->name }}</option>    
                @endforeach
          </select>    
        </div>
				<div class="col-md-6">
					<button id="btnSubmit" type="submit" class="btn btn-block btn-primary">
						<i class="fa fa-save"></i>
						<b>Simpan</b>
					</button>
				</div>
			</div>
		</div>
	</div>
</form>
<!-- MODAL INFO -->
<div id="info" class="modal fade bs-example-modal-sm" role="dialog" aria-hidden="true" style="display: none;">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
          <div id="minfo" class="row">
            <div class="text-center">
              <p>Sukses Menyimpan Transaksi</p>
              <p><a href="{{ url('buy_transactions') }}" class="btn btn-success">Konfirm</a></p>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>
@endsection
@section('scripts')
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<script src="{{ asset('vendors/select2/dist/js/select2.full.js') }}"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.js"></script> -->
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script src="{{ asset('js/validator.js') }}"></script>
<script src="{{ asset('vendors/moment/moment.js') }}"></script>
<script src="{{ asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="{{ asset('js/numeral/numeral.min.js') }}"></script>
<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script> -->
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script type="text/javascript">
  var id = 0;
	$(document).ready(function() {
		var CurrentDate = moment();
		$("#transDate").datetimepicker({
			defaultDate: CurrentDate
		});
    $("#discount_price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
	});

	$("#btnSubmit").click(function(e) {
		e.preventDefault();
		var items = {};
		var services = {};
		var i = 0;
    var status = $("#status").val() || 0;
    if (status == 0) {
      alert("Pilih status terlebih dahulu");
      return;
    }
		$("#itemsContainer tr").each(function (e) {
			var id = $(this).find('.itemSearch').val();
			var qty = $(this).find('.itemQty input').val();
      var price = $(this).find('.itemPrice input').val();
			if (id != null) {
					items[i] = {
					id: id,
					qty: qty,
          price: parseInt(price.replace(/,/g,''))
				};
				i++;
			}
    });
    i=0;
    $("#servicesContainer tr").each(function (e) {
			var id = $(this).find('.serviceSearch').val();
      var price = $(this).find('.servicePrice input[type=text]').val();
			if (id != null) {
					services[i] = {
					id: id,
          price: parseInt(price.replace(/,/g,''))
				};
				i++;
			}
    });
    if (jQuery.isEmptyObject(items) && jQuery.isEmptyObject(services)) {
      alert("Data tidak boleh kosong");
      return;
    }
    var supplier_name = $("#supplier_name").val();
    if (!supplier_name) {
      alert("Isi nama supplier terlebih dahulu");
      return;
    }
    var values = {
	  trans_date: $("#transDate").val(),
      items: JSON.stringify(items),
      services: JSON.stringify(services),
      total_price: getSubTotal(),
      discount_price: get_discount_price(),
      notes: $("textarea").val(),
      status: status,
      supplier_name: supplier_name,
      supplier_phone: $("#supplier_phone").val(),
      _token : "{{ csrf_token() }}"
    };
    $.ajax({
      type: "POST",
      url: "{{ url('buy_transactions/save') }}",
      dataType: "JSON",
      data: values,
      success: function(data) {
        $("#info").modal({
          backdrop: 'static',
          keyboard: false
        });
        $("#info").modal("show");
      },
      error: function(data) {
        alert("error");
      }
    });
	});

	$("body").on('click', '#addItem', function(e) {
		e.preventDefault();
	    var html = '<tr class="itemBox'+id+'" >' +
	                    '<td class="itemImage text-center"><span class="fa fa-shopping-cart"></span></td>' +
	                    '<td class="itemName"><select class="form-control itemSearch" data-id-item="'+id+'" name="itemId[]"></select></td>' +
	                    '<td class="itemQty"><input type="number" class="form-control itemQtyInput" name="itemQty[]" value="1" max="" min="1"></td>' +
	                    '<td class="itemAvailable text-center"><span>-</span></td>' +
                      '<td class="itemPrice"><input type="text" name"itemPrice[]" class="form-control"/></td>' + 
	                    // '<td class="itemPrice text-center"><span>-</span><input type="hidden" name="itemPrice[]" value=""></td>' +            
	                    '<td class="itemTotal text-center"><span>-</span></td>' +
	                    '<td class="text-center"><a href="#" class="itemDrop btn btn-rounded btn-danger btn-xs"><i class="fa fa-close"><i></a></td>' +
	                    '</tr>';
	    $("#itemsContainer").append(html);
        $('.itemBox'+id+' .itemSearch').select2({
          placeholder: "Cari produk ...",
          // minimumInputLength: 2,
          ajax: {
            url: "{{ url('items/api/buygetitems') }}",
            dataType: 'json',
            delay: 250,
            type: 'GET',
            data: function(params) {
              return {
                term: params.term
              }
            },
            processResults: function (data) {
              return {
                results: data
              };
          },
          cache: true
          }
        });
	   	id++;
	    $(this).text("+ Tambah produk lain");
	});

	$("body").on('change', '.itemSearch', function(e) {
		e.preventDefault();
		var id = $(this).attr('data-id-item');
        var itemBox = $('.itemBox'+id);
        var idPro = $(this).val();
        var isNew = true;
        $('.itemSearch').each(function(i) {
	        if ($(this).val() == idPro && $(this).attr('data-id-item') != id) {
	            $(this).parents('tr').find('.itemTotal span').text(function(){
	            	var qty = parseInt($(this).parents('tr').find('.itemQty input').val());
	            	var price = $(this).parents('tr').find('.itemPrice span').text();
	            	price = price.replace(/,/g,'');
					var total = qty * price;
	                return numeral(total).format(0,0);
	            });
	            isNew = false;
	        }
	    });

	    if (isNew == true){
	        $.ajax({
	                type: "POST",
	                url: "{{ url('items/api/itemattribute') }}",
	                data: {id: idPro, _token: '{{ csrf_token() }}'},
	                dataType: 'json',
	                success: function (data) {
	                    itemBox.find('.itemAvailable span').text(data.stock); 
	                    // itemBox.find('.itemPrice span').text(numeral(data.price).format(0,0));
	                    itemBox.find('.itemTotal span').text(numeral(data.price).format(0,0)); 
                      
                      itemBox.find('.itemPrice input[type=text]').val(numeral(data.price).format(0,0));
                      $(".itemPrice input[type=text]").autoNumeric('init',{aSep:',',aDec:'.', vMin: '0', vMax: '999999999'});
	                    calSummary();
	                }
	        });
	    } else{
	        $(this).parents("tr").fadeOut().remove();
	    }
	    calSummary();
	});

  

  $("body").on('keyup', '.itemPrice input[type=text]', function(event) {
        // this.value = this.value.replace(/[^0-9\.]/g,'');
      var price = $(this).val();
      var total = $(this).parent().closest('tr').children('.itemTotal').children('span'); 
      var qty = $(this).parent().closest('tr').children('.itemQty').children('input').val();
      price = price.replace(/,/g,'');
      calTotalItem(total, qty, price);
    });

	$("body").on("click", "#addService", function(e) {
		e.preventDefault();
		var html = '<tr class="serviceBox'+id+'" >' +
                    '<td class="serviceImage text-center"><span class="fa fa-gears"></span></td>' +
                    '<td class="serviceName"><select class="form-control serviceSearch" data-id-item="'+id+'" name="serviceId[]"></select></td>' +
                    '<td class="serviceQty"></td>' +
                    '<td class="serviceAvailable text-center"><span></span></td>' +
                    '<td class="servicePrice"><input type="text" name"servicePrice[]" class="form-control"/></td>' + 
                    // '<td class="servicePrice text-center"><span>-</span><input type="hidden" name="servicePrice[]" value=""></td>' +            
                    '<td class="serviceTotal text-center"><span></span></td>' +
                    '<td class="text-center"><a href="#" class="itemDrop btn btn-rounded btn-danger btn-xs"><i class="fa fa-close"><i></a></td>' +
                    '</tr>';
    	$("#servicesContainer").append(html);
        $('.serviceBox'+id+' .serviceSearch').select2({
          placeholder: "Cari jasa ...",
          // minimumInputLength: 2,
          ajax: {
            url: "{{ url('services/api/getservices') }}",
            dataType: 'json',
            delay: 250,
            type: 'GET',
            // data:data
            data: function(params) {
              return {
                term: params.term
              }
            },
            processResults: function (data) {
              return {
                results: data
              };
          },
          cache: true
          }
        });
	   	id++;
        $(this).text("+ Tambah jasa lain");
	});

	$("body").on('change', '.serviceSearch', function(e) {
		e.preventDefault();
		var id = $(this).attr('data-id-item');
        var itemBox = $('.serviceBox'+id);
        var idPro = $(this).val();
        var isNew = true;
        $('.serviceSearch').each(function(i) {
	        if ($(this).val() == idPro && $(this).attr('data-id-item') != id) {
	            isNew = false;
	        }
	    });

	    if (isNew == true) {
	        $.ajax({
	                type: "POST",
	                url: "{{ url('services/api/serviceattribute') }}",
	                data: {id: idPro, _token: '{{ csrf_token() }}'},
	                dataType: 'json',
	                success: function (data) {
                      itemBox.find('.servicePrice input[type=text]').val(numeral(data.price).format(0,0));
                      $(".servicePrice input[type=text]").autoNumeric('init',{aSep:',',aDec:'.', vMin: '0', vMax: '999999999'});
	                    // itemBox.find('.servicePrice input[type=text]').text().format(0,0));
	                    calSummary();
	                }
	        });
	    } else {
	        $(this).parents("tr").fadeOut().remove();
	    }
	});

    $("body").on('keyup', '.servicePrice input[type=text]', function(event) {
        // this.value = this.value.replace(/[^0-9\.]/g,'');
        calSummary();
    });

    $("body").on('keyup mouseup', '.itemQtyInput', function () {
	    var qty = $(this).val();
		var total = $(this).parent().closest('tr').children('.itemTotal').children('span'); 
		var price = $(this).parent().closest('tr').children('.itemPrice').children('input').val();
		price = price.replace(/,/g,'');
    calTotalItem(total, qty, price);

	});

  function calTotalItem(total, qty, price) {
		var xtot = qty*parseInt(price);
		total.text(numeral(xtot).format(0,0));
		calSummary();
  }

  $("#discount_price").change(function(){
    calSummary();
  });

  function getSubTotal() {
    var subtotal = $("#subTotal b").text();
    return parseInt(subtotal.replace(/[^0-9]/g, ''));
  }

	function calSummary() {
        var subTotal = 0;
        var val = 0;
        var vals = 0;
        $(".itemTotal span").each(function (e) {
            val = $(this).text();
            if(val == '-') val = '0';
            val = val.replace(/,/g,'');
            subTotal = subTotal + parseInt(val);
        });
        // $(".servicePrice span").each(function (e) {
        //     vals = $(this).text();
        //     if(vals == '-') vals = '0';
			  //     vals = vals.replace(/,/g,'');
        //     subTotal = subTotal + parseInt(vals);
        // });
        
    // $(".itemPrice input[type=text]").each(function (e) {
    //     vals = $(this).val();
    //     if(vals == '-') vals = '0';
    //     vals = vals.replace(/,/g,'');
    //     subTotal = subTotal + parseInt(vals);
    // });
        
    $(".servicePrice input[type=text]").each(function (e) {
        vals = $(this).val();
        if(vals == '-') vals = '0';
        vals = vals.replace(/,/g,'');
        subTotal = subTotal + parseInt(vals);
    });
        subTotal -= get_discount_price();
        $("#subTotal b").text("Rp. " + numeral(subTotal).format(0,0));              
    }

    function get_discount_price() {
        var discount_price = $("#discount_price").val() || '-';
        if(discount_price == '-') discount_price = '0';
        return parseInt(discount_price.replace(/[^0-9]/g, ''));
    }

	$('body').on('click', '.itemDrop', function (e) {
        e.preventDefault();
        var el = $(this).parent("td").parent("tr");
        if (el.find("select").val()) {
          if(confirm("Apa kamu yakin ingin menghapus produk/jasa ini? ? ")) el.fadeOut().remove();
        } else {
          el.fadeOut().remove();
        }
        if($("#itemsContainer tr").length == 0) {
      		$("#addItem").text("+ Add Item");
        }
        if($("#servicesContainer tr").length == 0) {
      		$("#addService").text("+ Add Service");
        }
        calSummary();
    });
</script>
@endsection