@extends('layouts.master')
@section('title', 'Sales Report')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <style>
    	.material-switch > input[type="checkbox"] {
		    display: none;   
		}

		.material-switch > label {
		    cursor: pointer;
		    height: 0px;
		    position: relative; 
		    width: 40px;  
		}

		.material-switch > label::before {
		    background: rgb(0, 0, 0);
		    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
		    border-radius: 8px;
		    content: '';
		    height: 16px;
		    margin-top: -8px;
		    position:absolute;
		    opacity: 0.3;
		    transition: all 0.4s ease-in-out;
		    width: 40px;
		}
		.material-switch > label::after {
		    background: rgb(255, 255, 255);
		    border-radius: 16px;
		    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
		    content: '';
		    height: 24px;
		    left: -4px;
		    margin-top: -8px;
		    position: absolute;
		    top: -4px;
		    transition: all 0.3s ease-in-out;
		    width: 24px;
		}
		.material-switch > input[type="checkbox"]:checked + label::before {
		    background: inherit;
		    opacity: 0.5;
		}
		.material-switch > input[type="checkbox"]:checked + label::after {
		    background: inherit;
		    left: 20px;
		}
    </style>
@endsection
@section('content')
	<!-- FORM ELEMENT -->
	<div class="row">
		<div class="col-sm-1">
			<div class="material-switch">
                <input id="all" name="all" type="checkbox" checked="" />
                <label for="all" class="label-primary"></label>
            </div>
            <h4>All</h4>
		</div>
		<div id="ocheck">
			<div class="col-sm-1">
				<div class="material-switch">
	                <input id="invoice" name="invoice" type="checkbox"/>
	                <label for="invoice" class="label-warning"></label>
	            </div>
            	<h4>Invoice</h4>
			</div>
			<div class="col-sm-1">
				<div class="material-switch">
	                <input id="paid" name="paid" type="checkbox"/>
	                <label for="paid" class="label-success"></label>
	            </div>
	            <h4>Paid</h4>
			</div>
			<div class="col-sm-1">
				<div class="material-switch">
	                <input id="cancel" name="cancel" type="checkbox"/>
	                <label for="cancel" class="label-danger"></label>
	            </div>
	            <h4>Cancel</h4>
			</div>
		</div>
		
		<div class="col-sm-2"></div>
		<div class="col-sm-2 text-right">
			<h4>Report by date range : </h4>
		</div>
		<div class="col-sm-2">
			<input id="date" name="date" type="text" class="form-control" name="daterange" value="" />
		</div>
		<div class="col-sm-1">
			<button id="show" class="btn btn-success">
				Show
			</button>
			<!-- <a href="#" class="btn btn-success"> Show</a> -->
		</div>
	</div>
	<!-- END FORM ELEMENT -->
	<!-- DATATABLE -->
	<div class="row" style="padding-top: 50px;">	
	</div>
	<div class="row">
		<table id="result" class="table table-hover table-sm dataTable no-footer">
			<thead>
				<tr>
					<th>No</th>
					<th>Time</th>
					<th>Invoice</th>
					<th>Item/Service</th>
					<th>Category</th>
					<th>Price</th>
					<th>Qty</th>
					<th>Total Price</th>
					<th>Status</th>
				</tr>
			</thead>
			<tfoot>
            <tr>
                <th colspan="7" style="text-align:right">Total : </th>
                <th></th>
            </tr>
        </tfoot>
		</table>
	</div>
	<!-- END DATATABLE -->
@endsection
@section('scripts')
	<!-- Datatables -->
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/dataTables.buttons.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/buttons.flash.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/jszip.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/pdfmake.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/vfs_fonts.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/buttons.html5.min.js') }}"></script>
  	<script src="{{ asset('js/datatables_buttons/buttons.print.min.js') }}"></script>
	<!-- <script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script> -->
	<script src="{{ asset('vendors/moment/moment.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
	<script src="{{ asset('js/numeral/numeral.min.js') }}"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
			var s = $('#date').daterangepicker();
			var table = $('#result').DataTable( {


            	"footerCallback": function ( row, data, start, end, display ) {
		            var api = this.api(), data;
		 
		            // Remove the formatting to get integer data for summation
		            var intVal = function ( i ) {
		                return typeof i === 'string' ?
		                    i.replace(/[\$,]/g, '')*1 :
		                    typeof i === 'number' ?
		                        i : 0;
		            };
		 
		            // Total over all pages
		            total = api
		                .column( 7 )
		                .data()
		                .reduce( function (a, b) {
		                    return intVal(a) + intVal(b);
		                }, 0 );
		 
		            // Total over this page
		            pageTotal = api
		                .column( 7, { page: 'current'} )
		                .data()
		                .reduce( function (a, b) {
		                    return intVal(a) + intVal(b);
		                }, 0 );
		 
		            // Update footer
		            $( api.column( 7 ).footer() ).html(
		                'Rp. ' + numeral(pageTotal).format(0,0) //pageTotal
		            );
		        },

				destroy: true,
		        "data": {!! json_encode($transaction_details) !!},
		        "columns": [
			        {
	                    "data": "id",
	                    render: function (data, type, row, meta) {
	                        return meta.row + meta.settings._iDisplayStart + 1;
	                    },
	                    "width": "1%",
	                    orderable: false, 
	                    searchable:false,
	                    "className": "text-center"
	                },
		            { "data": "date" },
		            { "data": "invoice_no" },
		            { "data": "item_service" },
		            { "data": "category" },
		            { "data": "price", render: $.fn.dataTable.render.number('.','.',0,'Rp. ') },
		            { "data": "qty" },
		            { "data": "total", render: $.fn.dataTable.render.number('.','.',0,'Rp. ') },
		            { "data": "status" }
		        ],
		        "searching": false,
		        "pageLength": 100,
		        "dom": 'Bfrtip',
		        "buttons": [
		            // 'copy'
		            // , 'csv'
		            // , 'excel'
		            // 'pdf'
		            // , 'excel'
		            // , 'print'
		            { "extend": 'pdf', "text":'Export PDF',"className": 'btn btn-default btn-xs' },
		            { "extend": 'print', "text":'Print',"className": 'btn btn-default btn-xs' }
		        ]
            	// "order": [[ 2, 'desc' ]]


		    } );
	    	table.on( 'order.dt search.dt', function () {
	            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	                cell.innerHTML = i+1;
	            } );
	        } ).draw();



	    	$("#show").click(function() {
	    		var all = $("#all").is(":checked");
	    		var invoice = $("#invoice").is(":checked");
	    		var paid = $("#paid").is(":checked");
	    		var cancel = $("#cancel").is(":checked");
	    		var date = $("#date").val();
	    		var values = {
	    			date: date,
	    			all: all,
	    			invoice: invoice,
	    			paid: paid,
	    			cancel: cancel,
	      			_token : "{{ csrf_token() }}"
	    		}
	    		$.ajax({
			      type: "POST",
			      url: "{{ url('report/transactions/show') }}",
			      dataType: "JSON",
			      data: values,
			      success: function(data) {
	    			table.clear();
	    			table.rows.add(data);
	    			table.draw();
			      },
			      error: function(data) {
			        console.log(data);
			        alert("error");
			      }
			    });
	    	});
	    	// var a = $(".dt-buttons");
	    	// var b = a.find("a");
	    	// b.addClass("btn btn-primary");
	    	// console.log(a);
	    	// console.log(b);

    	});
    	$("#all").change(function() {
    		if (this.checked) {
        		$("#ocheck :checkbox").removeAttr( "checked" );
		    }
    	});
    	$("#ocheck :checkbox").change(function() {
			if (this.checked) {
        		$("#all").removeAttr( "checked" );
		    }
    	});
    </script>
@endsection