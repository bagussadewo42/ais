@extends('layouts.master')
@section('title', 'Inventory Report')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <!-- <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet"> -->
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">
    <!-- bootstrap-daterangepicker -->
    <link href="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <!-- bootstrap-datetimepicker -->
    <link href="{{ asset('vendors/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.css') }}" rel="stylesheet">
    <style>
    	.material-switch > input[type="checkbox"] {
		    display: none;   
		}

		.material-switch > label {
		    cursor: pointer;
		    height: 0px;
		    position: relative; 
		    width: 40px;  
		}

		.material-switch > label::before {
		    background: rgb(0, 0, 0);
		    box-shadow: inset 0px 0px 10px rgba(0, 0, 0, 0.5);
		    border-radius: 8px;
		    content: '';
		    height: 16px;
		    margin-top: -8px;
		    position:absolute;
		    opacity: 0.3;
		    transition: all 0.4s ease-in-out;
		    width: 40px;
		}
		.material-switch > label::after {
		    background: rgb(255, 255, 255);
		    border-radius: 16px;
		    box-shadow: 0px 0px 5px rgba(0, 0, 0, 0.3);
		    content: '';
		    height: 24px;
		    left: -4px;
		    margin-top: -8px;
		    position: absolute;
		    top: -4px;
		    transition: all 0.3s ease-in-out;
		    width: 24px;
		}
		.material-switch > input[type="checkbox"]:checked + label::before {
		    background: inherit;
		    opacity: 0.5;
		}
		.material-switch > input[type="checkbox"]:checked + label::after {
		    background: inherit;
		    left: 20px;
		}
    </style>
@endsection
@section('content')
	<!-- FORM ELEMENT -->
	<div class="row">
		<!-- <div class="col-sm-1">
			<div class="material-switch">
                <input id="all" name="all" type="checkbox" checked="" />
                <label for="all" class="label-primary"></label>
            </div>
            <h4>All</h4>
		</div> -->
		<div id="ocheck">
			<div class="col-sm-1">
				<div class="material-switch">
	                <input id="items" name="items" type="checkbox" checked="" />
	                <label for="items" class="label-warning"></label>
	            </div>
            	<h4>Items</h4>
			</div>
			<div class="col-sm-1">
				<div class="material-switch">
	                <input id="services" name="services" type="checkbox" checked="" />
	                <label for="services" class="label-success"></label>
	            </div>
	            <h4>Services</h4>
			</div>
		</div>
		
		<div class="col-sm-5"></div>
		<div class="col-sm-2 text-right">
			<h4>Report Condition</h4>
		</div>
		<div class="col-sm-2">
			<select name="condition" id="condition" class="form-control" style="cursor: pointer;">
				<option value="other"></option>
				<option value="out_of_stock">Items Out Of Stock</option>
				<option value="best_seller">Best Seller</option>
				<option value="never_sold">Never Sold</option>
			</select>
			<!-- <input id="date" name="date" type="text" class="form-control" name="daterange" value="" /> -->
		</div>
		<div class="col-sm-1">
			<button id="show" class="btn btn-success">
				Show
			</button>
			<!-- <a href="#" class="btn btn-success"> Show</a> -->
		</div>
	</div>
	<!-- END FORM ELEMENT -->
	<!-- DATATABLE -->
	<div class="row" style="padding-top: 50px;">	
	</div>
	<div class="row">
		<table id="result" class="table table-hover table-sm dataTable no-footer">
			<thead>
				<tr>
					<th>No</th>
					<th>Name</th>
					<th>Type</th>
					<th>Category</th>
					<th>Applied Car</th>
					<th>Price</th>
					<th>Stock</th>
					<th>Deleted</th>
				</tr>
			</thead>
		</table>
	</div>
	<!-- END DATATABLE -->
@endsection
@section('scripts')
	<!-- Datatables -->
	<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.print.min.js"></script>
	<script src="{{ asset('vendors/moment/moment.js') }}"></script>
    <script src="{{ asset('vendors/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <!-- bootstrap-datetimepicker -->    
    <script src="{{ asset('vendors/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script type="text/javascript">
    	$(document).ready(function() {
			var s = $('#date').daterangepicker();
			var table = $('#result').DataTable( {
				destroy: true,
		        "data": {!! json_encode($items) !!},
		        "columns": [
			        {
	                    "data": "id",
	                    render: function (data, type, row, meta) {
	                        return meta.row + meta.settings._iDisplayStart + 1;
	                    },
	                    "width": "1%",
	                    orderable: false, 
	                    searchable:false,
	                    "className": "text-center"
	                },
		            { "data": "name" },
		            { "data": "type" },
		            { "data": "category" },
		            { "data": "applied_car" },
		            { "data": "price", render: $.fn.dataTable.render.number('.','.',0,'Rp. ') },
		            { "data": "stock" },
		            {
		            	"data": "is_deleted",
		            	render: function (data, type, row, meta) {
		            		return (data == true ? 'Yes':'');
		            	}
		            }
		        ],
		        "searching": false,
		        "pageLength": 100,
		        "dom": 'Bfrtip',
		        "buttons": [
		            { "extend": 'pdf', "text":'Export PDF',"className": 'btn btn-default btn-xs' },
		            { "extend": 'print', "text":'Print',"className": 'btn btn-default btn-xs' }
		        ]
            	// "order": [[ 2, 'desc' ]]
		    } );
	    	table.on( 'order.dt search.dt', function () {
	            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
	                cell.innerHTML = i+1;
	            } );
	        } ).draw();


	    	$("#show").click(function() {
	    		var all = $("#all").is(":checked");
	    		var items = $("#items").is(":checked");
	    		var services = $("#services").is(":checked");
	    		var condition = $("#condition").val();
	    		var values = {
	    			condition: condition,
	    			all: all,
	    			items: items,
	    			services: services,
	      			_token : "{{ csrf_token() }}"
	    		}
	    		$.ajax({
			      type: "POST",
			      url: "{{ url('report/inventory/show') }}",
			      dataType: "JSON",
			      data: values,
			      success: function(data) {
	    			table.clear();
	    			table.rows.add(data);
	    			table.draw();
			      },
			      error: function(data) {
			        console.log(data);
			        alert("error");
			      }
			    });
	    	});

    	});
    	// $("#all").change(function() {
    	// 	if (this.checked) {
     //    		$("#ocheck :checkbox").removeAttr( "checked" );
		   //  }
    	// });
    	$("#ocheck :checkbox").change(function() {
			if (this.checked) {
        		// $("#all").removeAttr( "checked" );
		    }
		    // if ($("#items").is(':checked') && $("#services").is(':checked')) {
		    // 	$("#ocheck :checkbox").removeAttr("checked");
		    // 	$("#all").prop('checked', true);
		    // }
    	});
    </script>
@endsection