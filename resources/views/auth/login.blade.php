<!doctype html>
<html lang="{{ app()->getLocale() }}">
  <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>AIS | Login</title>
      <link href="{{ asset('vendors/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
      <!-- Font Awesome -->
      <link href="{{ asset('vendors/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
      <!-- NProgress -->
      <link href="{{ asset('vendors/nprogress/nprogress.css') }}" rel="stylesheet">
      <link href="{{ asset('css/custom.min.css') }}" rel="stylesheet">
  </head>
  <body  class="login">
     <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="POST" action="{{ route('login') }}">
              {{ csrf_field() }}
              <h1>Login Form</h1>
               @if ($errors->has('email'))
                    <span class="help-block">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
               @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
              <div>
                <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="User Email" required="" autofocus="" />
              </div>
              <div>
                <input type="password" class="form-control" name="password" placeholder="User Password" required="" />
              </div>
              <div>
                <!-- <a class="btn btn-default submit" href="index.html">Log in</a> -->
                <button type="submit" class="btn btn-default submit">
                    Login
                </button>
                <!-- <a class="reset_pass" href="#">Lost your password?</a> -->
              </div>
              <div class="clearfix"></div>
              <div class="separator">
                <div>
                  <h1><i class="fa fa-automobile"></i> 99 AUTO </h1>
                  <p>©2022 All Rights Reserved 99 Auto</p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
  </body>
</html>
