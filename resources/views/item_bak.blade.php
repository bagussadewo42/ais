@extends('layouts.master')
@section('title', 'Items')
@section('styles')
    <!-- Datatables -->
    <link href="{{ asset('vendors/datatables.net-bs/css/dataTables.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/select2/dist/css/select2.min.css') }}" rel="stylesheet">
    <style type="text/css">
        .lebar {
            width: 600px !important;
        }
    </style>
@endsection
@section('content')
<div id="success-alert" class="alert alert-success alert-dismissible fade in" role="alert" style="display: none;">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
  </button>
  <strong>Success !</strong> Data has been saved
</div>
<div id="success-alert" class="alert alert-error" role="alert" style="display: none;">
  </button>
  <strong>Error !</strong> Something Happened
</div>

<div class="row">
        <table id="examples" class="table table-striped table-bordered table-hover" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Applied car</th>
                    <th>Price</th>
                    <th>Stock</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <div class="row">
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#addItem">Add New Item</button>
    </div>

    <!-- MODAL ADD -->
    <div id="addItem" class="modal fade bs-example-modal-lg" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add New Item</h4>
          </div>
          <div class="modal-body">
              <form id="add-item" class="form-horizontal form-label-left" data-toggle="validator">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="item-name">Item Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="item-name" name="item-name" class="form-control col-md-7 col-xs-12" autofocus required>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Category <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" id="item-category" name="item-category" required>
                      <option value="" >Select Category</option>  
                          @foreach($categories as $category)
                              <option value="{{ $category->id }}" >{{ $category->name }}</option>    
                          @endforeach
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Applied Car <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" id="item-car" name="item-car" multiple="multiple" required style="width: 100%">
                      <option value="Universal">Universal</option>
                      <option value="Avanza">Avanza</option>
                      <option value="Xenia">Xenia</option>
                      <option value="Sigra">Sigra</option>
                      <option value="Pajero Sport">Pajero Sport</option>
                      <option value="Fortuner">Fortuner</option>
                      <option value="Brio">Brio</option>
                      <option value="Mobilio">Mobilio</option>
                      <option value="Ertiga">Ertiga</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="item-price" class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="item-price" class="form-control col-md-7 col-xs-12" name="item-price" data-a-sign="Rp. " required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="item-stock" class="control-label col-md-3 col-sm-3 col-xs-12">Stock <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="item-stock" class="form-control col-md-7 col-xs-12" type="number" name="item-stock" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <input type="submit" name="submit" id="add-submit" class="btn btn-primary" value="Submit">
                     <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
                    <button class="btn btn-info"  data-dismiss="modal" type="button">Cancel</button>
                    <button class="btn btn-default" type="reset">Reset</button>
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>

        </div>
      </div>
    </div>

    <!-- MODAL EDIT -->
    <div id="editItem" class="modal fade bs-example-modal-lg" role="dialog" aria-hidden="true" style="display: none;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">

          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Add New Item</h4>
          </div>
          <div class="modal-body">
              <form id="edit-item" class="form-horizontal form-label-left">
                <input type="hidden" name="edit-id" value="" id="edit-id">
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="edit-name">Item Name <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input type="text" id="edit-name" name="edit-name" required class="form-control col-md-7 col-xs-12" autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Category <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" id="edit-category" name="edit-category" required>
                      <option value="" >Select Category</option>  
                          @foreach($categories as $category)
                              <option value="{{ $category->id }}" >{{ $category->name }}</option>    
                          @endforeach
                    </select>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12">Applied Car <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <select class="form-control" id="edit-car" name="edit-car" multiple="multiple" required style="width: 100%">
                      <option value="Universal">Universal</option>
                      <option value="Avanza">Avanza</option>
                      <option value="Xenia">Xenia</option>
                      <option value="Sigra">Sigra</option>
                      <option value="Pajero Sport">Pajero Sport</option>
                      <option value="Fortuner">Fortuner</option>
                      <option value="Brio">Brio</option>
                      <option value="Mobilio">Mobilio</option>
                      <option value="Ertiga">Ertiga</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="edit-price" class="control-label col-md-3 col-sm-3 col-xs-12">Price <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="edit-price" class="form-control col-md-7 col-xs-12" name="edit-price" data-a-sign="Rp. " required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="edit-stock" class="control-label col-md-3 col-sm-3 col-xs-12">Stock <span class="required">*</span></label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                    <input id="edit-stock" class="form-control col-md-7 col-xs-12" type="number" name="edit-stock" required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                    <input type="submit" name="submit" id="edit-submit" class="btn btn-primary" value="Submit">
                     <!-- id="post-add-item" type="submit" class="btn btn-primary">Submit</button> -->
                    <button class="btn btn-info"  data-dismiss="modal" type="button">Cancel</button>
                    <input type="button" id="delete-submit" name="delete-submit" value="Delete" class="btn btn-danger">
                  </div>
                </div>
              </form>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button> -->
          </div>

        </div>
      </div>
    </div>
@endsection
@section('scripts')
<!-- jQuery Tags Input -->
<script src="{{ asset('vendors/jquery.tagsinput/src/jquery.tagsinput.js') }}"></script>
<!-- Datatables -->
<script src="{{ asset('vendors/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-keytable/js/dataTables.keyTable.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js') }}"></script>
<script src="{{ asset('vendors/datatables.net-scroller/js/dataTables.scroller.min.js') }}"></script>
<script src="{{ asset('vendors/select2/dist/js/select2.min.js') }}"></script>
<script src="{{ asset('js/autoNumeric.js') }}"></script>
<script src="{{ asset('js/validator.js') }}"></script>


<script type="text/javascript">
	$(document).ready(function() {
        $("#add-item").validate({
          rules: {
              username: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              },
              password: {
                  minlength: 3,
                  maxlength: 15,
                  required: true
              }
          },
          highlight: function(element) {
              $(element).closest('.form-group').addClass('has-error');
          },
          unhighlight: function(element) {
              $(element).closest('.form-group').removeClass('has-error');
          },
          errorElement: 'span',
          errorClass: 'help-block',
          errorPlacement: function(error, element) {
              if (element.parent('.input-group').length) {
                  error.insertAfter(element.parent());
              } else {
                  error.insertAfter(element);
              }
          }
        });
        $("#item-price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        var format = function(num) {
        var str = num.toString().replace("Rp. ", ""), parts = false, output = [], i = 1, formatted = null;
        if(str.indexOf(".") > 0) {
          parts = str.split(".");
          str = parts[0];
        }
        str = str.split("").reverse();
        for(var j = 0, len = str.length; j < len; j++) {
          if(str[j] != ",") {
            output.push(str[j]);
            if(i%3 == 0 && j < (len - 1)) {
              output.push(",");
            }
            i++;
          }
        }
        formatted = output.reverse().join("");
        return("Rp. " + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };

        $('#item-car, #edit-car').select2();
        $('#item-car').val('Universal').trigger('change');
        var table = $('#examples').DataTable({
            responsive: true,
            serverSide: true,
            processing: true,
            ajax: 'items/anyitem',
            columns: [
                {
                    "data": "id",
                    render: function (data, type, row, meta) {
                        return meta.row + meta.settings._iDisplayStart + 1;
                    }
                },
                {data: 'name', name: 'name'},
                {data: 'category', name: 'category'},
                {data: 'applied_car', name: 'applied_car'},
                {data: 'price', name: 'price', render: $.fn.dataTable.render.number('.','.',0,'Rp. ')},
                {data: 'stock', name: 'stock'},
                {data: 'action', orderable: false, searchable:false, name:'id' }
            ],
            "columnDefs": [{
                "defaultContent": "-",
                "targets": "_all",
                "searchable": false,
                "orderable": false,
                "targets": 0
            }],
            "order": [[ 1, 'asc' ]]
        });
        table.on( 'order.dt search.dt', function () {
            table.column(0, {search:'applied', order:'applied'}).nodes().each( function (cell, i) {
                cell.innerHTML = i+1;
            } );
        } ).draw();

        // $("#item-price, #edit-price").keyup(function(e){
        //     $(this).val(format($(this).val()));
        // });

        $("#add-submit").click(function() {
            var name = $("#item-name").val();
            var category = $("#item-category").val();
            var cars = $("#item-car").val();
            var price = $("#item-price").autoNumeric('get');
            var stock = $("#item-stock").val();
            price = price.replace(/Rp. |,/g,'');
            var datas = {
              name: name,
              category: category,
              cars: cars.join(),
              price: price,
              stock: stock
            };
            console.log(datas);
            $.post("items/add", datas, function(data){ //$("#a dd-item").serialize(
                if(data.msg == 'success') {
                    table.ajax.reload();
                    $(".bs-example-modal-lg").modal('hide');
                    $('#add-item')[0].reset();
                    $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
                        $("#success-alert").slideUp(500);
                    });
                    // $("#success-alert").show();
                } else {
                    alert("Error insert data");
                }
            });
        });

        $("#edit-submit, #delete-submit").click(function() {
            var is_deleted = false;
            if (this.id == "delete-submit") {
              is_deleted = true;
            }
            console.log(is_deleted);
            var id = $("#edit-id").val();
            var name = $("#edit-name").val();
            var category = $("#edit-category").val();
            var cars = $("#edit-car").val();
            if ( !cars ) {
              console.log('hollo');
              cars = '';
            } else {
              console.log('holla');
              cars = cars.join();
            }
            var price = $("#edit-price").autoNumeric('get');
            var stock = $("#edit-stock").val();
            price = price.replace(/Rp. |,/g,'');
            var datas = {
              id: id,
              name: name,
              category: category,
              cars: cars,
              price: price,
              stock: stock,
              is_deleted: is_deleted
            };
            console.log(datas);
            $.post("items/edit", datas, function(data){ //$("#a dd-item").serialize(
                if(data.msg == 'success') {
                    table.ajax.reload();
                    $(".bs-example-modal-lg").modal('hide');
                    $('#add-item')[0].reset();
                    $("#success-alert").fadeTo(3000, 500).slideUp(500, function(){
                        $("#success-alert").slideUp(500);
                    });
                    // $("#success-alert").show();
                } else {
                    alert("Error insert data");
                }
            });
        });
        
        $('modal').on('shown.bs.modal', function(){
          $(this).find('[autofocus]').focus();
        });
    });

  // Edit row
  function editRow(id) {
    if ( 'undefined' != typeof id ) {
      $.getJSON('items/edit/' + id, function(obj) {
        $('#edit-id').val(id);
        $('#edit-name').val(obj.name);
        $('#edit-category').val(obj.category);
        $('#edit-car').val(obj.applied_car);
        $("#edit-price").autoNumeric('init',{aSep:'.',aDec:',', vMin: '0', vMax: '999999999'});
        $('#edit-price').val(obj.price);
        $('#edit-stock').val(obj.stock);
        $('#editItem').modal('show');
      }).fail(function() { alert('Unable to fetch data, please try again later.') });
    } else alert('Unknown row id.');
  }
      
</script>

@endsection