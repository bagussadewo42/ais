<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Item;
use App\Transaction;
use App\TransactionDetail;
use Mike42\Escpos\Printer; 
use Mike42\Escpos\PrintConnectors\WindowsPrintConnector;
use Barryvdh\Debugbar\Facade as Debugbar;

class TransactionsController extends Controller
{
	public function index()
	{
		return view('contents.transactions.transaction');
	}

	public function anytransaction()
	{
		$transactions = Transaction::alltrans();
        $datatables = Datatables::of($transactions)
                        ->addColumn('action', function($row) {
                        return '<a href="/transactions/detail/' . $row->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i> Lihat</a>';
        });
        return $datatables->make(true);
	}

	public function create()
	{
		$status = DB::table('tags')->where('type','TRANSACTION_STATUS')->get();
		$last_id = DB::table('transactions')->count() + 1;
		$invoice = "#INV$last_id";
		return view('contents.transactions.create', compact('invoice', 'status'));
	}

	public function save(Request $request)
	{
		$items = json_decode($request->get('items'));
		$services = json_decode($request->get('services'));
		$notes = $request->get('notes');
		$trans_date = $request->get('trans_date');
		$status = $request->get('status');
		$total_price = $request->get('total_price');
		$discount_price = $request->get('discount_price');
		$id = 0;
		try {
			$last_id = DB::table('transactions')->count() + 1;
        	$invoice = "#INV$last_id";
        	$timestamp = strtotime($trans_date);
			DB::beginTransaction();
				$new_date =  date("Y-m-d H:i:s", $timestamp);
				$transaction = Transaction::create([
					'invoice_no' => $invoice,
					'date' => $new_date, 
					'total_price' => $total_price, 
					'discount_price' => $discount_price, 
					'notes' => $notes,
					'status' => $status,
	    			'type' => 0
				]);
				$id = $transaction->id;
				foreach ($items as $key => $value) {
					$item_id = $value->id;
					$qty = $value->qty;
					if($qty < 1) continue;
					$price = $value->price;
					DB::table('transaction_details')->insert(array(
						'transaction_id' => $id,
						'item_id' => $item_id,
						'qty' => $qty,
						'services_id' => 0,
						'price' => $price
					));
					$item = Item::whereId($item_id)->firstOrFail();
					$item->stock = $item->stock - $qty;
					$item->save(); 
				}
				foreach ($services as $key => $value) {
					$service_id = $value->id;
					$price = $value->price;
					DB::table('transaction_details')->insert(array(
						'transaction_id' => $id,
						'item_id' => 0,
						'qty' => 1,
						'services_id' => $service_id,
						'price' => $price
					));
				}
			DB::commit();
		} catch(\Exception $e) {
			DB::rollback();
			echo $e->getMessage();
		}
		return json_encode($id);
	}

	public function detail($id)
	{
		$status = DB::table('tags')->where('type','TRANSACTION_STATUS')->get();
		$transaction = Transaction::whereId($id)->firstOrFail();
		$transaction_details = DB::table('transaction_details')->where('transaction_id', $id)->get();
		$items = DB::table('items')->where('is_deleted', FALSE)->orWhere('is_deleted', null)->get();
		$services = DB::table('services')->where('is_deleted', FALSE)->orWhere('is_deleted', null)->get();
		return view('contents.transactions.detail', compact('transaction', 'transaction_details', 'items', 'services', 'status'));
	}

	// public function print($id)
	// {
	// 	$transactions = Transaction::whereId($id)->firstOrFail();
	// 	$details = TransactionDetail::print_trx($id);
	// 	$transactions["details"] = $details;
	// 	return view('contents.transactions.print', compact('transactions'));
	// }

	public function print($id)
	{
		
		$transactions = Transaction::whereId($id)->firstOrFail();
		$details = TransactionDetail::print_trx($id);

		$connector = new WindowsPrintConnector("DigitUP");
		$printer = new Printer($connector);

		/* Name of shop */
		$printer -> setFont(Printer::FONT_B);
		$printer -> setJustification(Printer::JUSTIFY_CENTER);
		$printer -> selectPrintMode(Printer::MODE_DOUBLE_WIDTH);
		$printer -> text("OTO MART 99.\n");
		$printer -> setJustification(Printer::JUSTIFY_CENTER);
		$printer -> selectPrintMode();
		$printer -> text("Jl. Raya Ciantra No.1, Ciantra, Cikarang  Selatan, Bekasi. 17530.\n");
		$printer -> text("08170224623\n");		
		$printer -> text("------------------------------------------");
		$printer -> feed();

		
		// $printer -> setEmphasis(true);
		$printer -> text($transactions->invoice_no . "\n");
		// $printer -> setEmphasis(false);
		$printer -> text(date('Y-m-d H:i', strtotime($transactions->date)) . "\n");
		$printer -> text("------------------------------------------\n");

		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		$line = sprintf('%-15.40s %-3.3s %-10.40s %-10.40s', "Produk/Jasa", "Qty", "Harga", "Total");
		$printer -> text($line . "\n");
		
		$total_price = 0;
		foreach ($details as $item) {
			$line = sprintf('%-15.40s %-3.3s %-10.40s %-10.40s', $this->print_item_name($item->item_name), $item->qty, $item->price, number_format($item->total_price));
			
			$printer -> text($line);
			$printer -> text("\n");
			$total_price += $item->total_price;
		}
		$printer -> setJustification(Printer::JUSTIFY_RIGHT);
		$printer -> text("=============\n");
		$printer -> setJustification(Printer::JUSTIFY_LEFT);
		$line = sprintf('%-15.40s %-3.3s %-10.40s %-10.40s', "Total", "", "", number_format($total_price));
		$printer -> text($line . "\n");

		if ($transactions->discount_price) {
			$line = sprintf('%-15.40s %-3.3s %-10.40s %-10.40s', "Potongan harga", "", "", number_format($transactions->discount_price));
			$printer -> text($line . "\n");
			$printer -> setJustification(Printer::JUSTIFY_RIGHT);
			$printer -> text("=============\n");
			$printer -> setJustification(Printer::JUSTIFY_LEFT);
			$line = sprintf('%-15.40s %-3.3s %-10.40s %-10.40s', "Sub total", "", "", number_format($transactions->total_price));
			$printer -> text($line . "\n");
		}
		$printer -> text("------------------------------------------\n");
		
		$printer -> setJustification(Printer::JUSTIFY_CENTER);
		$printer -> text("Terima kasih sudah belanja di toko kami :)\n");

		$printer -> cut();
		$printer -> close();
			return view('contents.transactions.print');
	}

	public static function print_item_name($item_name)
	{
		if (strlen($item_name) > 15) {
			$ww = wordwrap($item_name,15,PHP_EOL);
			$names = explode(PHP_EOL, $ww);
			$i = 1;
			$result = "";
			foreach($names as $name) {
				if ($i == count($names)) {
					$sc = strlen($name);
					$name = $name . str_repeat(' ', 15-$sc);
					$result .= $name;
				} else {
					$result .= $name;
					$result .= "\n";
				}
				$i++;
			}
			return $result;
		} else {
			return $item_name;
		}
	}

	public function update(Request $request)
	{
		$trans_id = $request->get('trans_id');
		$items = json_decode($request->get('items'));
		$services = json_decode($request->get('services'));
		$trans_date = $request->get('trans_date');
		$notes = $request->get('notes');
		$status = $request->get('status');
		try {
			DB::beginTransaction();
				$transaction = Transaction::whereId($trans_id)->firstOrFail();
				$timestamp = strtotime($trans_date);
				$transaction->date = date("Y-m-d H:i:s", $timestamp);
				$transaction->notes = $notes;
				$transaction->status = $status;
				$transaction->save();
				$transaction_details = DB::table('transaction_details')->where('transaction_id', $transaction->id)->get();
				foreach ($items as $item) {
					if($item->item_id < 0 ) {
						DB::table('transaction_details')->insert(array(
							'transaction_id' => $transaction->id,
							'item_id' => $item->id,
							'qty' => $item->qty,
							'services_id' => 0,
							'price' => $item->price
						));
						$item1 = Item::whereId($item_id)->firstOrFail();
						$item1->stock = $item->stock - $item->qty;
						$item1->save(); 
						continue;
					}
					foreach ($transaction_details as $td) {
						if($td->id == $item->item_id) {
							$trans_det = TransactionDetail::whereId($td->id)->firstOrFail();
							$c_qty = $trans_det->qty;
							$trans_det->qty = $item->qty;
							$trans_det->is_deleted = $item->is_deleted;
							$trans_det->save();
							$item2 = Item::whereId($item->id)->firstOrFail();
							$item2->stock = $item2->stock - ($trans_det->qty - $c_qty);
							$item2->save();
							break;
						}
					}
				}
				foreach ($services as $service) {
					if($service->service_id < 0 ) {
						DB::table('transaction_details')->insert(array(
							'transaction_id' => $transaction->id,
							'item_id' => 0,
							'qty' => 1,
							'services_id' => $service->id,
							'price' => $service->price
						));
						continue;
					}
					foreach ($transaction_details as $td) {
						if($td->id == $service->service_id) {
							$trans_det = TransactionDetail::whereId($td->id)->firstOrFail();
							$trans_det->is_deleted = $service->is_deleted;
							$trans_det->save();
							break;
						}
					}
				}
			DB::commit();
		} catch(\Exception $e) {
			DB::rollback();
			echo $e->getMessage();
		}
		return json_encode($items);
	}

    public function destroy($id)
    {
        $item = Transaction::whereId($id)->firstOrFail();
        $item->is_deleted = TRUE;
        $item->save();
        return redirect('transactions')->with('status', 'Transaksi berhasil dihapus!');
    }

	public function report()
	{
		$transaction_details = Transaction::reports();
		return view('contents.reports.sales', compact('transaction_details'));
	}

	public function show(Request $request)
	{
		$date = $request->get('date');
		$all = $request->get('all');
		$invoice = $request->get('invoice');
		$paid = $request->get('paid');
		$cancel = $request->get('cancel');
		$start_date = substr($date, 0, 10);
		$end_date = substr($date, 13, 10);
		$transactions = Transaction::show($start_date, $end_date, $all, $invoice, $paid, $cancel);
		return json_encode($transactions);
	}
}
