<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Transaction;
use App\Item;
use Barryvdh\Debugbar\Facade as Debugbar;

use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = DB::table('items')->where('is_deleted', false)->orWhere('is_deleted', null)->count();
        $services = DB::table('services')->where('is_deleted', false)->orWhere('is_deleted', null)->count();
        $transactions = DB::table('transactions')->where('is_deleted', false)->orWhere('is_deleted', null)->count();
        $out_of_stock = Item::out_of_stock();
        $revenue = Transaction::weekly_revenue();
        $sales = Transaction::weekly_sales();
        $ratio = Transaction::product_ratio();
        Debugbar::info("ini out of stock : ", $out_of_stock);
        return view('welcome', compact('items', 'services', 'transactions', 'revenue', 'sales', 'ratio', 'out_of_stock'));
    }

    public function profile()
    {
        return view('contents.profile');
    }

    public function update(Request $request)
    {
        $name = $request->get('name');
        $old_password = $request->get('old_password');
        $new_password = $request->get('new_password');
        $user = Auth::user();
        $user->name = $name;
        if ($new_password) {
            if (!Hash::check($old_password, $user->password)) {
                echo "not same " . bcrypt($old_password);
                return redirect('profile')->with('error','Old password does not match');
            }
            if (strlen($new_password) < 6) return redirect('profile')->with('error','New password must be at least 6 characters');
            $user->password = bcrypt($new_password);
        }
        $user->save();
        echo "hoho";
        return redirect('profile')->with('success','Success Update Profile');
    }

    public function deploy()
    {
        // $command = 'cd ~/auto99 && git pull origin master'; // On Production
        // if(env('APP_DEBUG')) {
        $command = 'cd .. && git pull origin master'; // On Development
        // }
        
        $result = $this->execPrint($command);
        return view('deploy', compact('result'));
    }

    function execPrint($command) {
        $res = array();
        exec($command, $res);
        $result = "";
        foreach ($res as $line) {
            $result .= $line;
        }
        return $result;
    }
}
