<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;
use App\Tag;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class ServicesController extends Controller
{
    public function index()
    {
        return view('contents.services.service');
    }

    public function anyservice()
    {
    	// $services = DB::table('services')->where('is_deleted', FALSE)->orWhere('is_deleted', null)->orderBy('id', 'desc')->get();
        $services = Service::getServiceList();
        $datatables = Datatables::of($services)
                        ->addColumn('action', function($row) {
                        return '<a href="/services/edit/' . $row->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Ubah</a>';
        });
        return $datatables->make(true);
    }

    public function create()
    {
        $categories = Tag::getDataSources('SERVICE_CATEGORIES','');
        return view('contents.services.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $num = $request->get('price');
        $replaced = str_replace_array('Rp. ', [''], $num);
        $price = str_replace_array('.', [''], $replaced);
        $service = new Service(array(
            'name' => $request->get('name'),
            'category' => $request->get('category'),
            'price' => $price,
        ));
        $service->save();
        return redirect('services')->with('status', 'Jasa berhasil ditambahkan');
    }

    public function edit($id)
    {
        $service = Service::whereId($id)->firstOrFail();
        $categories = Tag::getDataSources('SERVICE_CATEGORIES','');
        return view('contents.services.edit', compact('service', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $num = $request->get('price');
        $replaced = str_replace_array('Rp. ', [''], $num);
        $price = str_replace_array('.', [''], $replaced);

        $service = Service::whereId($id)->firstOrFail();
        $service->name = $request->get('name');
        $service->category = $request->get('category');
        $service->price = $price;
        $service->is_deleted = filter_var($request->get('is_deleted'), FILTER_VALIDATE_BOOLEAN);
        $service->save();
        return redirect('services')->with('status', 'Jasa berhasil diubah');
    }


    public function destroy($id)
    {
        $service = Service::whereId($id)->firstOrFail();
        $service->is_deleted = TRUE;
        $service->save();
        return redirect('services')->with('status', 'Jasa berhasil dihapus!');
    }

    public function restore($id)
    {
        $item = Service::whereId($id)->firstOrFail();
        $item->is_deleted = FALSE;
        $item->save();
        return redirect('items')->with('status', 'Jasa berhasil dikembalikan!');
    }

    public function getservices(Request $request)
    {
        $term = $request->get('term');
        $services = Service::getServiceWhere($term);
        return json_encode($services);
    }

    public function serviceattribute(Request $request)
    {
        $id = $request->get('id');
        $service = Service::whereId($id)->firstOrFail();
        return json_encode($service);
    }

    public function trash()
    {
        return view('contents.trash.service');
    }

    public function anyservicetrash()
    {
        $items = Service::where('is_deleted', true)->get();
        $datatables = Datatables::of($items)
                        ->addColumn('action', function($row) {
                        return '<a href="javascript:restore(' . $row->id . ');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Ubah</a>';
        });
        return $datatables->make(true);
    }
}
