<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Tag;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Barryvdh\Debugbar\Facade as Debugbar;
use Illuminate\Support\Facades\Input;

class ItemController extends Controller
{
    public function index()
    {
        $categories = Tag::getDataSources('categories','');
        return view('contents.items.item', compact('categories'));
    }

    public function anyitem()
    {
        $items = Item::getItemList();
        $datatables = Datatables::of($items)
                        ->addColumn('action', function($row) {
                        return '<a href="/items/edit/' . $row->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Ubah</a>';
        });
        return $datatables->make(true);
    }

    public function create()
    {
        $categories = Tag::getDataSources('ITEM_CATEGORIES','');
        $cars = Tag::getDataSources('CAR_VENDOR','');
        return view('contents.items.create', compact('categories', 'cars'));
    }

    public function store(Request $request)
    {
        $num = $request->get('price');
        $replaced = str_replace_array('Rp. ', [''], $num);
        $price = preg_replace('/[.,]/', '', $replaced);
        
        $num_last_buy_price = $request->get('last_buy_price');
        $replaced_last_buy_price = str_replace_array('Rp. ', [''], $num_last_buy_price);
        $last_buy_price = preg_replace('/[.,]/', '', $replaced_last_buy_price);

        $arr = array();
        $input = Input::all();
        $cars = $input["car"];
        foreach ($cars as $car) {
            array_push($arr, $car);
        }
        $applied_car = join(',', $arr);

        $item = new Item(array(
            'name' => $request->get('name'),
            'category' => $request->get('category'),
            'applied_car' => $applied_car,
            'price' => $price,
            'last_buy_price' => $last_buy_price,
            'stock' => $request->get('stock')
        ));
        $item->save();
        return redirect('items')->with('status', 'Produk berhasil ditambahkan!');
    }

    public function edit($id)
    {
        $item = Item::whereId($id)->firstOrFail();
        $categories = Tag::getDataSources('ITEM_CATEGORIES','');
        $cars = Tag::getDataSources('CAR_VENDOR','');
        $cars_selected =  explode(',', $item->applied_car);
        return view('contents.items.edit', compact('item', 'categories', 'cars', 'cars_selected'));
    }

    public function update(Request $request, $id)
    {
        $num = $request->get('price');
        $replaced = str_replace_array('Rp. ', [''], $num);
        $price = preg_replace('/[.,]/', '', $replaced);
        
        $num_last_buy_price = $request->get('last_buy_price');
        $replaced_last_buy_price = str_replace_array('Rp. ', [''], $num_last_buy_price);
        $last_buy_price = preg_replace('/[.,]/', '', $replaced_last_buy_price);

        $arr = array();
        $input = Input::all();
        $cars = $input["car"];
        foreach ($cars as $car) {
            array_push($arr, $car);
        }
        $applied_car = join(',', $arr);

        $item = Item::whereId($id)->firstOrFail();
        $item->name = $request->get('name');
        $item->category = $request->get('category');
        $item->applied_car = $applied_car;
        $item->price = $price;
        $item->last_buy_price = $last_buy_price;
        // $item->stock = $request->get('stock');
        $item->is_deleted = filter_var($request->get('is_deleted'), FILTER_VALIDATE_BOOLEAN);
        $item->save();
        return redirect('items')->with('status', 'Produk berhasil diubah!');
    }

    public function destroy($id)
    {
        $item = Item::whereId($id)->firstOrFail();
        $item->is_deleted = TRUE;
        $item->save();
        return redirect('items')->with('status', 'Produk berhasil dihapus!');
    }

    public function restore($id)
    {
        $item = Item::whereId($id)->firstOrFail();
        $item->is_deleted = FALSE;
        $item->save();
        return redirect('items')->with('status', 'Produk berhasil dikembalikan!');
    }

    public function getitems(Request $request)
    {
        $term = $request->get('term');
        $items = Item::getItemWhere($term);
        Debugbar::info($request->get('term'));
        Debugbar::info(json_encode($items));
        return json_encode($items);
    }

    public function buygetitems(Request $request)
    {
        $term = $request->get('term');
        $items = Item::buyGetItemWhere($term);
        Debugbar::info($request->get('term'));
        Debugbar::info(json_encode($items));
        return json_encode($items);
    }

    public function itemattribute(Request $request)
    {
        $id = $request->get('id');
        $items = Item::whereId($id)->firstOrFail();
        Debugbar::info('id is : ' . $id);
        return json_encode($items);
    }

    public function report()
    {
        $items = Item::report(true,true);
        return view('contents.reports.inventory', compact('items'));
    }

    public function show(Request $request)
    {
        $condition = $request->get('condition');
        // $all = $request->get('all');
        $items = $request->get('items');
        $services = $request->get('services');
        Debugbar::info($condition);
        // $data = new Array();
        switch ($condition) {
            case 'out_of_stock':
                $data = Item::report_out_of_stock();
                break;
            case 'best_seller':
                $data = Item::report_best_seller($items, $services);
                break;
            case 'never_sold':
                $data = Item::report_never_sold($items, $services);
                break;
            default:
                $data = Item::report($items, $services);
                break;
        }
        return json_encode($data);
    }

    public function trash()
    {
        return view('contents.trash.item');
    }

    public function anyitemtrash()
    {
        $items = Item::where('is_deleted', true)->get();
        $datatables = Datatables::of($items)
                        ->addColumn('action', function($row) {
                        return '<a href="javascript:restore(' . $row->id . ');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Ubah</a>';
        });
        return $datatables->make(true);
    }
}
