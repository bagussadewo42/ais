<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tag;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class TagsController extends Controller
{
    public function index()
    {
        return view('tags');
    }

    public function anytag()
    {
        $tags = DB::table('tags')->where('is_deleted', false)->orWhere('is_deleted', null)->orderBy('type', 'asc')->get();
        // $tags = Tag::getItemList();
        $datatables = Datatables::of($tags)
                        ->addColumn('action', function($row) {
                        // return '<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editItem">Add New Item</button>';
                        return '<a href="javascript:editRow(' . $row->id . ');" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> Edit</a>';
        });
        return $datatables->make(true);
    }

    public function store(Request $request)
    {
        $tag = new Tag(array(
            'name' => $request->get('name'),
            'type' => $request->get('type'),
            'subtype' => $request->get('subtype')
        ));

        $tag->save();
        return response()->json([
            'msg' => 'success',
            'message' => 'Data has been saved'
            ], 200);
    }

    public function edit($id)
    {
        $tag = Tag::whereId($id)->firstOrFail();
        return json_encode($tag);
    }

    public function update(Request $request)
    {
        $tag = Tag::whereId($request->get('id'))->firstOrFail();
        $tag->name = $request->get('name');
        $tag->type = $request->get('type');
        $tag->subtype = $request->get('subtype');
        $tag->is_deleted = filter_var($request->get('is_deleted'), FILTER_VALIDATE_BOOLEAN);
        $tag->save();
        return response()->json([
            'msg' => 'success',
            'message' => 'Data has been updated'
            ], 200);
    }

    public function destroy(Request $request)
    {
        $service = Tag::whereId($request->get('id'))->firstOrFail();
        $service->is_deleted = TRUE;
        $service->save();
        return response()->json([
            'msg' => 'success',
            'message' => 'Data has been updated'
            ], 200);
    }
}
