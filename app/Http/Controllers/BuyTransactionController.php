<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use App\Item;
use App\BuyTransaction;
use App\BuyTransactionDetail;

class BuyTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('contents.buy_transactions.buy_transaction');
    }

	public function anytransaction()
	{
		$transactions = BuyTransaction::alltrans();
        $datatables = Datatables::of($transactions)
                        ->addColumn('action', function($row) {
                        return '<a href="/buy_transactions/detail/' . $row->id . '" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-eye-open"></i> Lihat</a>';
        });
        return $datatables->make(true);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$status = DB::table('tags')->where('type','TRANSACTION_STATUS')->get();
		$last_id = DB::table('buy_transactions')->count() + 1;
		$invoice = "#INVB$last_id";
		return view('contents.buy_transactions.create', compact('invoice', 'status'));
    }

	public function save(Request $request)
	{
		$items = json_decode($request->get('items'));
		$services = json_decode($request->get('services'));
		$notes = $request->get('notes');
		$trans_date = $request->get('trans_date');
		$status = $request->get('status');
		$total_price = $request->get('total_price');
		$discount_price = $request->get('discount_price');
		try {
			$last_id = DB::table('buy_transactions')->count() + 1;
        	$invoice = "#INVB$last_id";
        	$timestamp = strtotime($trans_date);
			DB::beginTransaction();
				$new_date =  date("Y-m-d H:i:s", $timestamp);
				$transaction = BuyTransaction::create([
					'invoice_no' => $invoice,
					'supplier_name' => $request->get('supplier_name'),
					'supplier_phone' => $request->get('supplier_phone'),
					'date' => $new_date, 
					'total_price' => $total_price, 
					'discount_price' => $discount_price, 
					'notes' => $notes,
					'status' => $status,
	    			'type' => 0
				]);
				$id = $transaction->id;
				foreach ($items as $key => $value) {
					$item_id = $value->id;
					$qty = $value->qty;
					if($qty < 1) continue;
					$price = $value->price;
					DB::table('buy_transaction_details')->insert(array(
						'buy_transaction_id' => $id,
						'item_id' => $item_id,
						'qty' => $qty,
						'services_id' => 0,
						'price' => $price
					));
					$item = Item::whereId($item_id)->firstOrFail();
					$item->stock = $item->stock + $qty;
					$item->last_buy_price = $price;
					$item->save(); 
				}
				foreach ($services as $key => $value) {
					$service_id = $value->id;
					$price = $value->price;
					DB::table('buy_transaction_details')->insert(array(
						'transaction_id' => $id,
						'item_id' => 0,
						'qty' => 1,
						'services_id' => $service_id,
						'price' => $price
					));
				}
			DB::commit();
		} catch(\Exception $e) {
			DB::rollback();
			echo $e->getMessage();
		}
		return json_encode($items);
	}
    

	public function detail($id)
	{
		$status = DB::table('tags')->where('type','TRANSACTION_STATUS')->get();
		$transaction = BuyTransaction::whereId($id)->firstOrFail();
		$transaction_details = DB::table('buy_transaction_details')->where('buy_transaction_id', $id)->get();
		$items = DB::table('items')->where('is_deleted', FALSE)->orWhere('is_deleted', null)->get();
		$services = DB::table('services')->where('is_deleted', FALSE)->orWhere('is_deleted', null)->get();
		return view('contents.buy_transactions.detail', compact('transaction', 'transaction_details', 'items', 'services', 'status'));
	}

    public function destroy($id)
    {
        $item = BuyTransaction::whereId($id)->firstOrFail();
        $item->is_deleted = TRUE;
        $item->save();
        return redirect('buy_transactions')->with('status', 'Transaksi berhasil dihapus!');
    }
}
