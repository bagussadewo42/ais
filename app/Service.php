<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Service extends Model
{
    protected $table = 'services';
    protected $guarded = ['id'];

    public static function getServiceList()
    {
    	$query = "SELECT x1.id
					, x1.name
					, x2.name AS category
					, x1.price
					, x1.is_deleted
				FROM services x1
					LEFT JOIN tags x2
					ON x1.category = x2.id";
		return DB::select($query);
    }

	public static function getServiceWhere($name)
	{
		$query = "SELECT x1.id, CONCAT(x1.name, ' - ', x2.name) AS text 
		FROM services x1
			JOIN tags x2
			ON x1.category = x2.id
		WHERE x1.name LIKE '%$name%' 
		AND COALESCE(x1.is_deleted, FALSE) = FALSE";

		return DB::select($query);
	}
}
