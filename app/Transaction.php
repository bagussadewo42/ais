<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Barryvdh\Debugbar\Facade as Debugbar;

class Transaction extends Model
{
    //
    protected $table = 'transactions';
    protected $guarded = ['id'];

    public static function alltrans()
    {
		$query = "SELECT x1.*, x2.name AS status FROM transactions x1 JOIN tags x2 ON x1.status = x2.id";
		return DB::select($query);
    }

	public static function trx_by_id($id) {
		$query = "SELECT x1.*, x2.name AS status FROM transactions x1 JOIN tags x2 ON x1.status = x2.id
					WHERE x1.id = $id LIMIT 1";
		return DB::select($query);
	}

    public static function weekly_revenue()
    {
    	$query = "SELECT SUM(x1.price*x1.qty) AS total
				FROM transaction_details x1
					LEFT JOIN transactions x2
					ON x1.transaction_id = x2.id
					LEFT JOIN tags x3
					ON x2.status = x3.id
				WHERE (
					DATE_FORMAT( x2.date, '%Y-%m-%d' )>= DATE_FORMAT( DATE_SUB( NOW( ), INTERVAL 7 DAY ), '%Y-%m-%d' )
					AND DATE_FORMAT(x2.date, '%Y-%m-%d') <= DATE_FORMAT( NOW(), '%Y-%m-%d' )
				) AND LOWER(x3.name) NOT LIKE '%cancel%' AND COALESCE(x2.is_deleted,FALSE)=FALSE";
		return DB::select($query);
    }

    public static function weekly_sales()
    {
    	$query = "SELECT DATE_FORMAT( x2.date, '%Y-%m-%d' ) AS dates
					, SUM(x1.price*x1.qty) AS total
				FROM transaction_details x1
					LEFT JOIN transactions x2
					ON x1.transaction_id = x2.id
					LEFT JOIN tags x3
					ON x2.status = x3.id
				WHERE (
					DATE_FORMAT( x2.date, '%Y-%m-%d' )>= DATE_FORMAT( DATE_SUB( NOW( ), INTERVAL 7 DAY ), '%Y-%m-%d' )
					AND DATE_FORMAT(x2.date, '%Y-%m-%d') <= DATE_FORMAT( NOW(), '%Y-%m-%d' )
				) AND LOWER(x3.name) NOT LIKE '%cancel%' AND COALESCE(x2.is_deleted,FALSE)=FALSE
				GROUP BY 1";
		return DB::select($query);
    }

    public static function product_ratio()
    {
    	$query = "SELECT 'Produk' AS type
					, COUNT(item_id) AS total
				FROM transaction_details
				WHERE item_id >= 1 AND COALESCE(is_deleted,FALSE)=FALSE
				UNION ALL
				SELECT 'Jasa' AS type
					, COUNT(services_id) AS total
				FROM transaction_details
				WHERE services_id >= 1 AND COALESCE(is_deleted,FALSE)=FALSE";
		return DB::select($query);
    }

    public static function reports()
    {
    	$query = "SELECT x2.id 
    				, x2.date
					, x2.invoice_no
					, COALESCE(x4.name, x3.name) AS item_service
					, COALESCE(x5.name,'Service') as category
					, x1.price
					, x1.qty
					, (x1.price*x1.qty) AS total
					, COALESCE(x6.name,'') AS status
				FROM transaction_details x1
				LEFT JOIN transactions x2
					ON x1.transaction_id = x2.id
				LEFT JOIN items x3
					ON x1.item_id = x3.id
				LEFT JOIN services x4
					ON x1.services_id = x4.id
				LEFT JOIN tags x5
					ON x3.category = x5.id
				LEFT JOIN tags x6
					ON x2.status = x6.id
				WHERE COALESCE(x2.is_deleted, FALSE) = FALSE
				ORDER BY date DESC";
		return DB::select($query);
    }

    public static function show($start_date, $end_date, $all, $invoice, $paid, $cancel)
    {
    	$stat = "";
    	$eq = " 1<>1";
    	if( filter_var($invoice, FILTER_VALIDATE_BOOLEAN) ) {
    		$stat .= " LOWER(x6.name) LIKE '%Tagihan%' OR";
    	}
    	if(filter_var($paid, FILTER_VALIDATE_BOOLEAN) ) {
    		$stat .= " LOWER(x6.name) LIKE '%Dibayar%' OR";
    	}
    	if(filter_var($cancel, FILTER_VALIDATE_BOOLEAN) ) {
    		$stat .= " LOWER(x6.name) LIKE '%Dibatalkan%' OR";
    	}
		$filter = " AND ( " . $stat . " )";
    	if(filter_var($all, FILTER_VALIDATE_BOOLEAN) ) {
    		$stat = "";
    		$eq = " 1=1";
    	}
    	$status = " AND (
			" . $stat . $eq . "
    	)";
    	$date = "AND (
			      	DATE_FORMAT(x2.date, '%Y-%m-%d') >= DATE_FORMAT(STR_TO_DATE('$start_date', '%m/%d/%Y'), '%Y-%m-%d')
			      	AND DATE_FORMAT(x2.date, '%Y-%m-%d') <= DATE_FORMAT(STR_TO_DATE('$end_date', '%m/%d/%Y'), '%Y-%m-%d')
      			)";
    	$query = "SELECT x2.id 
    				, x2.date
					, x2.invoice_no
					, COALESCE(x4.name, x3.name) AS item_service
					, COALESCE(x5.name,'Service') AS category
					, x1.price
					, x1.qty
					, (x1.price*x1.qty) AS total
					, COALESCE(x6.name,'') AS status
				FROM transaction_details x1
				LEFT JOIN transactions x2
					ON x1.transaction_id = x2.id
					AND COALESCE(x2.is_deleted,FALSE) = FALSE
				LEFT JOIN items x3
					ON x1.item_id = x3.id
				LEFT JOIN services x4
					ON x1.services_id = x4.id
				LEFT JOIN tags x5
					ON x3.category = x5.id
				LEFT JOIN tags x6
					ON x2.status = x6.id
				WHERE COALESCE(x1.is_deleted, FALSE) = FALSE
				" . $status . $date . "
				ORDER BY date DESC";
		return DB::select($query);
    }
}
