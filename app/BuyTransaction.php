<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class BuyTransaction extends Model
{
    protected $table = 'buy_transactions';
    protected $guarded = ['id'];
    
    public static function alltrans()
    {
		$query = "SELECT x1.*, x2.name AS status FROM buy_transactions x1 JOIN tags x2 ON x1.status = x2.id";
		return DB::select($query);
    }
}
