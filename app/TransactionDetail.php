<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class TransactionDetail extends Model
{
    //
    protected $table = 'transaction_details';
    protected $guarded = ['id'];
    
	public static function print_trx($id) {
		$query = "SELECT x1.transaction_id
					, COALESCE(x2.name, x3.name) AS item_name
					, COALESCE(x1.qty,1) AS qty
					, x1.price
					, (x1.price*COALESCE(x1.qty,1)) AS total_price
					FROM transaction_details        x1
					LEFT JOIN items 				x2 ON x1.item_id = x2.id
					LEFT JOIN services 				x3 ON x1.services_id = x3.id
					WHERE x1.transaction_id = $id";
		return DB::select($query);
	}
}
