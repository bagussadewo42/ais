<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyTransactionDetail extends Model
{
    protected $table = 'buy_transaction_details';
    protected $guarded = ['id'];
}
