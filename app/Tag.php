<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Tag extends Model
{
    protected $table = 'tags';
    protected $guarded = ['id'];

	public static function getDataSources($type, $subtype) 
	{
		$query = "SELECT id, name FROM tags WHERE type = '$type' AND COALESCE(subtype,'') = '$subtype'";
		return DB::select($query);
	}
}
