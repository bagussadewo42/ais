<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Barryvdh\Debugbar\Facade as Debugbar;

class Item extends Model
{
    protected $table = 'items';
    protected $guarded = ['id'];

	public static function getItemList() 
	{
		$query = "SELECT x1.id
					, x1.name
					, x2.name AS category
					, x1.applied_car
					, x1.price
					, x1.last_buy_price
					, x1.stock
					, x1.is_deleted
				FROM items x1
					LEFT JOIN tags x2
					ON x1.category = x2.id";

		return DB::select($query);
	}

	public static function getItemWhere($name)
	{
		$query = "SELECT id, name AS text FROM items WHERE name LIKE '%$name%' AND COALESCE(is_deleted, FALSE) = FALSE AND stock <> 0";
		return DB::select($query);
	}

	public static function buyGetItemWhere($name)
	{
		$query = "SELECT id, name AS text FROM items WHERE name LIKE '%$name%' AND COALESCE(is_deleted, FALSE) = FALSE";
		return DB::select($query);
	}


	public static function out_of_stock()
	{
		$query = "SELECT * FROM items WHERE stock = 0 AND COALESCE(is_deleted, FALSE) = FALSE";
		return DB::select($query);
	}

	public static function report($items, $services)
	{
		$filter_item = "";
		$filter_service = "";
		if( !filter_var($items, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_item = " WHERE x1.id < 1";
		}
		if( !filter_var($services, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_service = " WHERE x3.id < 1";
		}
		$query = "SELECT x1.id
					, x1.name
					, 'Items' AS type
					, x2.name AS category
					, x1.applied_car
					, x1.price
					, x1.stock
					, COALESCE(x1.is_deleted, FALSE) is_deleted
				FROM items x1
				LEFT JOIN tags x2
					ON x1.category = x2.id
				" . $filter_item . "
				UNION ALL
				SELECT x3.id
					, x3.name
					, 'Services' AS type
					, x4.name AS category
					, '-' AS applied_car
					, x3.price
					, '-' AS stock
					, COALESCE(x3.is_deleted, FALSE) is_deleted
				FROM services x3
				LEFT JOIN tags x4
					ON x3.category = x4.id
				" . $filter_service;
		return DB::select($query);
	}

	public static function report_out_of_stock()
	{
		$query = "SELECT x1.id
					, x1.name
					, 'Items' AS type
					, x2.name AS category
					, x1.applied_car
					, x1.price
					, x1.stock
					, COALESCE(x1.is_deleted, FALSE) is_deleted
				FROM items x1
				LEFT JOIN tags x2
					ON x1.category = x2.id
				WHERE x1.stock < 1 AND COALESCE(x1.is_deleted,FALSE)=FALSE";
		return DB::select($query);
	}

	public static function report_best_seller($items, $services)
	{
		$filter_item = "";
		$filter_service = "";
		if( !filter_var($items, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_item = " AND x2.id < 1";
		}
		if( !filter_var($services, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_service = " AND x3.id < 1";
		}
		$query = "SELECT x1.item_id
					, x2.name
					, 'Items' AS type
					, x3.name AS category
					, x2.applied_car
					, x2.price
					, x2.stock
					, COALESCE(x2.is_deleted, FALSE) is_deleted
					, COUNT(x1.item_id) AS total
				FROM transaction_details x1
				LEFT JOIN items x2
					ON x1.item_id = x2.id
				LEFT JOIN tags x3
					ON x2.category = x3.id
				WHERE COALESCE(item_id,0) >= 1
				" . $filter_item . "
				GROUP BY 1,2,3,4,5,6,7,8
				UNION ALL
					SELECT x1.services_id
					, x2.name
					, 'Services' AS type
					, x3.name AS category
					, '-' AS applied_car
					, x2.price
					, '-' AS stock
					, COALESCE(x2.is_deleted, FALSE) is_deleted
					, COUNT(x1.services_id) AS total
				FROM transaction_details x1
				LEFT JOIN services x2
					ON x1.services_id = x2.id
				LEFT JOIN tags x3
					ON x2.category = x3.id
				WHERE COALESCE(services_id,0) >= 1
				" . $filter_service . "
				GROUP BY 1,2,3,4,5,6,7,8
				ORDER BY 9 DESC";
		return DB::select($query);
	}

	public static function report_never_sold($items, $services)
	{
		$filter_item = "";
		$filter_service = "";
		if( !filter_var($items, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_item = " AND x1.id < 1";
		}
		if( !filter_var($services, FILTER_VALIDATE_BOOLEAN) ) {
			$filter_service = " AND x4.id < 1";
		}
		$query = "SELECT x1.id
					, x1.name
					, 'Items' AS type
					, x3.name AS category
					, x1.applied_car
					, x1.price
					, x1.stock
					, x1.is_deleted
				FROM items x1
				LEFT JOIN transaction_details x2
					ON x1.id = x2.item_id
				LEFT JOIN tags x3
					ON x1.category = x3.id
				WHERE COALESCE(x2.id,'') = ''
				" . $filter_item . "
				UNION ALL
				SELECT x4.id
					, x4.name
					, 'Services' AS type
					, x6.name AS category
					,'-' AS applied_car
					, x4.price
					, '-' AS stock
					, x4.is_deleted
				FROM services x4
				LEFT JOIN transaction_details x5
					ON x4.id = x5.services_id
				LEFT JOIN tags x6
					ON x4.category = x6.id
				WHERE COALESCE(x5.id,'') = ''
				" . $filter_service . "";
		return DB::select($query);
	}
}
