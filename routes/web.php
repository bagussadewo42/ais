<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('login');
// });

Auth::routes();
Route::middleware(['auth'])->group(function () {

	// Home
	Route::get('/', 'HomeController@index')->name('home');
	Route::get('home', 'HomeController@index')->name('home');
	Route::get('deploy', 'HomeController@deploy')->name('deploy');

	// Profile
	Route::get('/profile', 'HomeController@profile');
	Route::post('/profile', 'HomeController@update');

	// Items
	Route::get('items', 'ItemController@index')->name('items');
	Route::get('items/anyitem', 'ItemController@anyitem');
	Route::get('items/create', 'ItemController@create');
	Route::post('items/create', 'ItemController@store');
	Route::get('items/edit/{id?}', 'ItemController@edit');
	Route::post('items/edit/{id?}', 'ItemController@update');
	Route::post('items/delete/{id?}', 'ItemController@destroy');
	Route::post('items/restore/{id?}', 'ItemController@restore');

	// Services
	Route::get('services', 'ServicesController@index')->name('services');
	Route::get('services/anyservice', 'ServicesController@anyservice');
	Route::get('services/create', 'ServicesController@create');
	Route::post('services/create', 'ServicesController@store');
	Route::get('services/edit/{id?}', 'ServicesController@edit');
	Route::post('services/edit/{id?}', 'ServicesController@update');
	Route::post('services/delete/{id?}', 'ServicesController@destroy');
	Route::post('services/restore/{id?}', 'ServicesController@restore');

	// Tags
	Route::get('tags', 'TagsController@index');
	Route::get('tags/anytag', 'TagsController@anytag');
	Route::post('tags/add', 'TagsController@store');
	Route::get('tags/edit/{id?}', 'TagsController@edit');
	Route::post('tags/edit', 'TagsController@update');
	Route::post('tags/delete', 'TagsController@destroy');

	// Transactions
	Route::get('transactions', 'TransactionsController@index')->name('transactions');
	Route::get('transactions/anytransaction', 'TransactionsController@anytransaction');
	Route::get('transactions/create', 'TransactionsController@create');
	Route::post('transactions/save', 'TransactionsController@save');
	Route::get('transactions/detail/{id?}', 'TransactionsController@detail');
	Route::get('transactions/detail/print/{id?}', 'TransactionsController@print');
	Route::post('transactions/update', 'TransactionsController@update');
	Route::post('transactions/delete/{id?}', 'TransactionsController@destroy');
	
	// Buy Transactions
	Route::get('buy_transactions', 'BuyTransactionController@index')->name('buy_transactions');
	Route::get('buy_transactions/anytransaction', 'BuyTransactionController@anytransaction');
	Route::get('buy_transactions/create', 'BuyTransactionController@create');
	Route::post('buy_transactions/save', 'BuyTransactionController@save');
	Route::get('buy_transactions/detail/{id?}', 'BuyTransactionController@detail');
	Route::post('buy_transactions/update', 'BuyTransactionController@update');
	Route::post('buy_transactions/delete/{id?}', 'BuyTransactionController@destroy');

	// API
	Route::get('items/api/getitems', 'ItemController@getitems')->name('getitems');
	Route::get('items/api/buygetitems', 'ItemController@buygetitems')->name('buygetitems');
	Route::post('items/api/itemattribute', 'ItemController@itemattribute');
	Route::get('services/api/getservices', 'ServicesController@getservices')->name('getservices');
	Route::post('services/api/serviceattribute', 'ServicesController@serviceattribute');

	// Reports
	Route::get('report/transactions', 'TransactionsController@report');
	Route::post('report/transactions/show', 'TransactionsController@show');
	Route::get('report/inventory', 'ItemController@report');
	Route::post('report/inventory/show', 'ItemController@show');

	// Trash
	// Route::get('trash/items', 'ItemController@trash');
	// Route::get('trash/anyitemtrash', 'ItemController@anyitemtrash');
	// Route::post('trash/items/restore', 'ItemController@restore');
	// Route::get('trash/services', 'ServicesController@trash');
	// Route::get('trash/anyservicetrash', 'ServicesController@anyservicetrash');
	// Route::post('trash/services/restore', 'ServicesController@restore');
});

