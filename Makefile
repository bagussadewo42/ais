run:
	php artisan serve
migrate:
	php artisan migrate:refresh --seed
docker:
	docker-compose up